package com.listener

import com.usahaq.usahaq.databinding.FragmentRegister1Binding
import com.usahaq.usahaq.databinding.FragmentRegister2Binding
import com.usahaq.usahaq.databinding.FragmentRegister3Binding
import java.io.File

interface RegisterActivityListener {
    fun fragment1(binding: FragmentRegister1Binding)
    fun fragment2(binding: FragmentRegister2Binding)
    fun fragment3(binding: FragmentRegister3Binding)
    fun fragment2ImageChanged(file: File)
}
