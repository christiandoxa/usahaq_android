package com.listener

import com.model.Transaction

interface TransactionInterface {
    fun setTransaction(transaction: Transaction)
}
