package com.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.usahaq.model.CommonResponseModel
import com.repo.repository.machine_learning.MachineLearningRepository
import com.repo.response.MachineLearningResponse

class MachineLearningViewModel(private val repository: MachineLearningRepository) : ViewModel() {
    fun getPrediction(type: String): LiveData<CommonResponseModel<MachineLearningResponse>> =
        repository.getPricePrediction(type)
}
