package com.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.usahaq.model.CommonResponseModel
import com.repo.repository.account.AccountRepository
import com.repo.response.AccountResponse

class LoginViewModel(private val userRepository: AccountRepository) : ViewModel() {

    fun signIn(): LiveData<CommonResponseModel<AccountResponse>> = userRepository.signIn()

}