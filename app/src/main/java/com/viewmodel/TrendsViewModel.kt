package com.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.usahaq.model.CommonResponseModel
import com.repo.repository.trends.TrendsRepository
import com.repo.request.TrendsRequest
import com.repo.response.TrendsResponse
import com.repo.response.TrendsSuggestionResponse
import com.repo.response.TrendsTodayResponse
import com.repo.response.TrendsYearResponse

class TrendsViewModel(private val trendsRepository: TrendsRepository) : ViewModel() {
    fun getTrends(trendsRequest: TrendsRequest): LiveData<CommonResponseModel<TrendsResponse>> =
        trendsRepository.trends(trendsRequest)

    fun getTrendsToday(): LiveData<CommonResponseModel<TrendsTodayResponse>> =
        trendsRepository.trendsToday()

    fun getTrendsSuggestion(): LiveData<CommonResponseModel<TrendsSuggestionResponse>> =
        trendsRepository.trendsSuggestions()

    fun getTrendsYear(year: String): LiveData<CommonResponseModel<TrendsYearResponse>> =
        trendsRepository.trendsYear(year)
}
