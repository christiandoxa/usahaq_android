package com.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.usahaq.model.CommonResponseModel
import com.repo.repository.transaction.TransactionRepository
import com.repo.request.TransactionRequest
import com.repo.response.MultipleTransactionResponse
import com.repo.response.PaymentResponse
import com.repo.response.TransactionResponse

class TransactionViewModel(private val transactionRepository: TransactionRepository) : ViewModel() {
    fun createTransaction(transactionRequest: TransactionRequest): LiveData<CommonResponseModel<TransactionResponse>> =
        transactionRepository.createTransaction(transactionRequest)

    fun getAllTransaction(isSell: Boolean? = null): LiveData<CommonResponseModel<MultipleTransactionResponse>> =
        transactionRepository.getAllTransaction(isSell)

    fun getAllPaymentMethod(): LiveData<CommonResponseModel<PaymentResponse>> =
        transactionRepository.getAllPaymentMethod()
}