package com.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.usahaq.model.CommonResponseModel
import com.repo.repository.product.ProductRepository
import com.repo.request.ProductRequest
import com.repo.response.MultipleProductResponse
import com.repo.response.ProductResponse

class ProductViewModel(private val productRepository: ProductRepository) : ViewModel() {
    fun createProduct(productRequest: ProductRequest): LiveData<CommonResponseModel<ProductResponse>> =
        productRepository.create(productRequest)

    fun getAllProduct(idBusiness: String): LiveData<CommonResponseModel<MultipleProductResponse>> =
        productRepository.getAllProduct(idBusiness)
}