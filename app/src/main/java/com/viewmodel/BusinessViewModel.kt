package com.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.usahaq.model.CommonResponseModel
import com.repo.repository.business.BusinessRepository
import com.repo.request.BusinessRequest
import com.repo.response.BusinessResponse
import com.repo.response.MultipleBusinessResponse

class BusinessViewModel(private val businessRepository: BusinessRepository) : ViewModel() {
    fun createBusiness(businessRequest: BusinessRequest): LiveData<CommonResponseModel<BusinessResponse>> =
        businessRepository.create(businessRequest)

    fun getAllBusiness(): LiveData<CommonResponseModel<MultipleBusinessResponse>> =
        businessRepository.getAllBusiness()
}
