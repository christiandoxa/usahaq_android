package com.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.usahaq.model.CommonResponseModel
import com.repo.repository.account.AccountRepository
import com.repo.request.AccountRequest
import com.repo.response.AccountResponse

class RegisterViewModel(private val userRepository: AccountRepository) : ViewModel() {

    fun create(accountRequest: AccountRequest): LiveData<CommonResponseModel<AccountResponse>> =
        userRepository.create(accountRequest)
}