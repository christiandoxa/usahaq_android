package com.example.usahaq.model

data class CommonResponseModel<T>(
    var isProcessingError: Boolean = false,
    var isInternalError: Boolean = false,
    var isNotFound: Boolean = false,
    var isConflict: Boolean = false,
    var isUnauthorized: Boolean = false,
    var isBadRequest: Boolean = false,
    var message: String? = null,
    var data: T? = null
)
