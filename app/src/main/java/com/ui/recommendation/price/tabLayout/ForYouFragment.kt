package com.ui.recommendation.price.tabLayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.recommendation.price.PricePredictionAdapter
import com.adapter.recommendation.price.PredictionAdapter
import com.example.usahaq.model.CommonResponseModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.model.MachineLearningData
import com.repo.response.MachineLearningResponse
import com.usahaq.usahaq.databinding.ActivityPriceReccomendationBinding
import com.usahaq.usahaq.databinding.FragmentForYouBinding
import com.usahaq.usahaq.databinding.SheetBinding
import com.util.DateHelper
import com.util.ViewModelFactory
import com.viewmodel.MachineLearningViewModel

class ForYouFragment : Fragment(), PricePredictionAdapter.OnClickListener {

    private lateinit var binding: FragmentForYouBinding
    private lateinit var sheetDialog: BottomSheetDialog
    private lateinit var activityBinding: ActivityPriceReccomendationBinding
    private lateinit var sheetBehaviour: BottomSheetBehavior<View>
    private lateinit var sheet: SheetBinding
    private lateinit var viewModel: MachineLearningViewModel
    private lateinit var adapter: PricePredictionAdapter
    private lateinit var shimmerFrameLayout : ShimmerFrameLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentForYouBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val factory = ViewModelFactory.getInstance(requireActivity().application)
        viewModel = ViewModelProvider(this, factory)[MachineLearningViewModel::class.java]
        sheet = SheetBinding.inflate(layoutInflater)
        activityBinding = ActivityPriceReccomendationBinding.inflate(layoutInflater)
        sheetBehaviour = BottomSheetBehavior.from(activityBinding.bottomSheet)
        sheetDialog = BottomSheetDialog(requireContext())
        sheetDialog.setContentView(sheet.root)
        adapter = PricePredictionAdapter(this)
        binding.rvItem.adapter = adapter
        binding.rvItem.layoutManager = LinearLayoutManager(requireContext())
        shimmerFrameLayout = binding.shimmerLayout
        shimmerFrameLayout.visibility = View.VISIBLE
        shimmerFrameLayout.startShimmer()
        fetchAllData()
    }

    private fun fetchAllData() {
        fetchData("chilli")
        fetchData("meat")
        fetchData("chicken")
        fetchData("onion")
        fetchData("big_red_chilli")
        fetchData("garlic")
        fetchData("rice")
        fetchData("sugar")
        fetchData("oil")
        fetchData("egg")
    }

    private fun fetchData(modelType: String) {
        viewModel.getPrediction(modelType).observe(viewLifecycleOwner, { result ->
            processResponseData(result, modelType)
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE
        })
    }

    private fun processResponseData(
        result: CommonResponseModel<MachineLearningResponse>,
        modelType: String
    ) {
        when {
            result.isUnauthorized -> {
                Toast.makeText(context, "Unauthorized", Toast.LENGTH_SHORT).show()
            }
            result.isBadRequest -> {
                Toast.makeText(context, "Bad request", Toast.LENGTH_SHORT).show()
            }
            result.isInternalError -> {
                Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
            }
            result.isProcessingError -> {
                Toast.makeText(context, "Error processing data", Toast.LENGTH_SHORT).show()
            }
            result.data != null -> {
                adapter.machineLearningDataList.add(
                    MachineLearningData(
                        modelType,
                        result.data!!.result
                    )
                )
                adapter.notifyDataSetChanged()
            }
        }
    }

    private fun showBottomSheetDialog(machineLearningData: MachineLearningData) {
        if (sheetBehaviour.state == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviour.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        sheet.btnClose.setOnClickListener {
            sheetDialog.dismiss()
        }
        sheetDialog.window?.statusBarColor = 0x04000000
        sheet.detail.text = DateHelper.getCurrentDate()
        sheet.tvRion.text = machineLearningData.name
        sheet.tvPrice.text = machineLearningData.result[0].toString()
        val result = machineLearningData.result.toMutableList() as ArrayList
        result.removeAt(0)
        val predictionAdapter = PredictionAdapter()
        predictionAdapter.machineLearningDataList = result
        sheet.rvDate.adapter = predictionAdapter
        sheet.rvDate.layoutManager = LinearLayoutManager(requireContext())
        sheetDialog.show()
    }

    override fun onClick(machineLearningData: MachineLearningData) {
        showBottomSheetDialog(machineLearningData)
    }

}