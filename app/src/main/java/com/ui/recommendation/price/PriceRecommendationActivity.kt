package com.ui.recommendation.price

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.usahaq.usahaq.databinding.ActivityPriceReccomendationBinding

class PriceRecommendationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPriceReccomendationBinding
    private lateinit var adapter: PriceRecommendationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPriceReccomendationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapter = PriceRecommendationAdapter(this@PriceRecommendationActivity)
        binding.viewpagerDetail.adapter = adapter
        binding.viewpagerDetail.offscreenPageLimit = adapter.itemCount
        TabLayoutMediator(
            binding.tableLayoutDetail,
            binding.viewpagerDetail
        ) { tab, position ->
            when (position) {
                0 -> tab.text = "For you"
                1 -> tab.text = "Market"
                2 -> tab.text = "Trending"
            }
        }.attach()
        binding.btnBack.setOnClickListener { onBackPressed() }
    }
}