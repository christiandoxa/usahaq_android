package com.ui.recommendation.price.tabLayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.usahaq.usahaq.databinding.FragmentTrendingBinding

class TrendingFragment : Fragment() {

    private lateinit var binding : FragmentTrendingBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTrendingBinding.inflate(layoutInflater)
        return binding.root
    }
}