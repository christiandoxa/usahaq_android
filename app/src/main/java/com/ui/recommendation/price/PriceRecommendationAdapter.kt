package com.ui.recommendation.price

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ui.recommendation.price.tabLayout.ForYouFragment
import com.ui.recommendation.price.tabLayout.MarketFragment
import com.ui.recommendation.price.tabLayout.TrendingFragment

class PriceRecommendationAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ForYouFragment()
            1 -> MarketFragment()
            2 -> TrendingFragment()
            else -> ForYouFragment()
        }
    }
}