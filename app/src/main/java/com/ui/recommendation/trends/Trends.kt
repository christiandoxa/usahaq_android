package com.ui.recommendation.trends

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.recommendation.trends.TrendsTodayAdapter
import com.adapter.recommendation.trends.TrendsYearsAdapter
import com.facebook.shimmer.ShimmerFrameLayout
import com.repo.response.TrendsTodayResponse
import com.synnapps.carouselview.ImageListener
import com.ui.recommendation.trends.explore.ExploreActivity
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ActivityTrendsBinding
import com.util.ViewModelFactory
import com.viewmodel.TrendsViewModel

class Trends : AppCompatActivity() {

    private lateinit var binding : ActivityTrendsBinding
    private lateinit var adapter : TrendsTodayAdapter
    private lateinit var viewModel: TrendsViewModel
    private var trendsTodayResponse : TrendsTodayResponse ?= null
    private lateinit var yearsAdapter: TrendsYearsAdapter
    private lateinit var shimmerFrameLayout : ShimmerFrameLayout

    private var sampleImages = intArrayOf(
        R.drawable.explore,
        R.drawable.compare,
        R.drawable.find
    )

    private var imageListener: ImageListener =
        ImageListener { position, imageView -> imageView?.setImageResource(sampleImages[position]) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrendsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        adapter = TrendsTodayAdapter()
        yearsAdapter = TrendsYearsAdapter()
        yearsAdapter.setData()
        shimmerFrameLayout = binding.shimmerLayout
        shimmerFrameLayout.visibility = View.VISIBLE
        binding.rvTrendsToday.visibility = View.GONE
        shimmerFrameLayout.startShimmer()

        val factory = ViewModelFactory.getInstance(this.application)
        viewModel = ViewModelProvider(this, factory)[TrendsViewModel::class.java]
        fetchTodayTrendsData()
        binding.rvTrendsToday.layoutManager = LinearLayoutManager(this)
        binding.rvTrendsToday.adapter = adapter
        binding.rvYearInSearch.layoutManager = LinearLayoutManager(this)
        binding.rvYearInSearch.adapter = yearsAdapter
        binding.btnBack.setOnClickListener {
            onBackPressed()
        }

        val carouselView = binding.carouselView
        carouselView.pageCount = sampleImages.size
        carouselView.setImageListener(imageListener)
        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                processTrends(query!!)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })
    }

    private fun processTrends(query: String) {
        val intent = Intent(this, ExploreActivity::class.java)
        intent.putExtra(ExploreActivity.QUERY_DATA, query)
        startActivity(intent)
    }

    private fun fetchTodayTrendsData() {
        viewModel.getTrendsToday().observe(this, { result ->
            when {
                result.isUnauthorized -> {
                    Toast.makeText(this, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                result.isBadRequest -> {
                    Toast.makeText(this, "Bad request", Toast.LENGTH_SHORT).show()
                }
                result.isInternalError -> {
                    Toast.makeText(this, "Server error", Toast.LENGTH_SHORT).show()
                }
                result.isProcessingError -> {
                    Toast.makeText(this, "Error processing data", Toast.LENGTH_SHORT).show()
                }
                result.data != null -> {
                    trendsTodayResponse = result.data
                    adapter.trendsTodayList = trendsTodayResponse!!.trendsToday
                    adapter.notifyDataSetChanged()
                    shimmerFrameLayout.stopShimmer()
                    shimmerFrameLayout.visibility = View.GONE
                    binding.rvTrendsToday.visibility = View.VISIBLE
                }
            }
        })
    }

}