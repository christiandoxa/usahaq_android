package com.ui.recommendation.trends.explore

import android.app.Dialog
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.recommendation.trends.CompareAdapter
import com.adapter.recommendation.trends.ComparisonAdapter
import com.facebook.shimmer.ShimmerFrameLayout
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.model.CommonTrendModel
import com.repo.request.TrendsRequest
import com.repo.response.TrendsData
import com.usahaq.usahaq.databinding.ActivityExploreBinding
import com.usahaq.usahaq.databinding.SheetAddCompareBinding
import com.usahaq.usahaq.databinding.SheetFilterBinding
import com.usahaq.usahaq.databinding.SheetRelatedQueryBinding
import com.util.AxisDateFormatter
import com.util.ViewModelFactory
import com.viewmodel.TrendsViewModel

class ExploreActivity : AppCompatActivity() {

    private lateinit var binding: ActivityExploreBinding
    private lateinit var compareAdapter: CompareAdapter
    private lateinit var comparisonAdapter: ComparisonAdapter
    private lateinit var filterDialog: Dialog
    private lateinit var filterBinding: SheetFilterBinding
    private lateinit var relatedDialog: Dialog
    private lateinit var relatedQueryBinding: SheetRelatedQueryBinding
    private lateinit var addCompareSheet : SheetAddCompareBinding
    private lateinit var addCompareDialog : Dialog
    private lateinit var viewModel: TrendsViewModel
    private lateinit var shimmerFrameLayout : ShimmerFrameLayout
    private val keywords = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityExploreBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val factory = ViewModelFactory.getInstance(application)
        viewModel = ViewModelProvider(this, factory)[TrendsViewModel::class.java]
        shimmerFrameLayout = binding.shimmerLayout
        showShimmer(true)
        if(keywords.isEmpty()){
            startFetchTrends()
        }

        binding.btnBack.setOnClickListener {
            onBackPressed()
        }

        binding.tvCompare.setOnClickListener {
            addQuery()
        }

    }

    private fun showShimmer(state : Boolean){
        if(state){
            binding.svCompare.visibility = View.GONE
            shimmerFrameLayout.visibility = View.VISIBLE
            shimmerFrameLayout.startShimmer()
        }
        else{
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility= View.GONE
            binding.svCompare.visibility= View.VISIBLE
        }
    }

    private fun startFetchTrends(){
        val query = intent.getStringExtra(QUERY_DATA)
        keywords.addAll(query!!.split(','))
        fetchTrends()
    }

    fun deleteKeywords(position : Int){
        keywords.removeAt(position)
        showShimmer(true)
        fetchTrends()
    }

    private fun fetchTrends() {

        viewModel.getTrends(TrendsRequest(keywords)).observe(this, { result ->
            when {
                result.isUnauthorized -> {
                    Toast.makeText(this, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                result.isBadRequest -> {
                    Toast.makeText(this, "Bad request", Toast.LENGTH_SHORT).show()
                }
                result.isInternalError -> {
                    Toast.makeText(this, "Server error", Toast.LENGTH_SHORT).show()
                }
                result.isProcessingError -> {
                    Toast.makeText(this, "Error processing data", Toast.LENGTH_SHORT).show()
                }
                result.data != null -> {
                    loadChart(keywords, result.data!!.trends)
                    compareAdapter = CompareAdapter(this)
                    compareAdapter.topicList = keywords
                    comparisonAdapter = ComparisonAdapter(this)
                    comparisonAdapter.setData(keywords, result.data!!.trends)
                    binding.apply {
                        btnBack.setOnClickListener { onBackPressed() }
                        rvCompare.adapter = compareAdapter
                        rvCompare.layoutManager = LinearLayoutManager(this@ExploreActivity)
                        cvFilter.setOnClickListener { showFilterDialog() }
                        rvComparison.adapter = comparisonAdapter
                        rvComparison.layoutManager = LinearLayoutManager(this@ExploreActivity)
                        showShimmer(false)
                    }
                }
            }
        })
    }

    private fun showFilterDialog() {
        filterBinding = SheetFilterBinding.inflate(layoutInflater)
        filterDialog = Dialog(this)
        filterDialog.setContentView(filterBinding.root)
        filterDialog.show()

        filterBinding.btnClose.setOnClickListener {
            filterDialog.dismiss()
        }
    }

    fun addQuery() {
        addCompareSheet = SheetAddCompareBinding.inflate(layoutInflater)
        addCompareDialog = Dialog(this)
        addCompareDialog.setContentView(addCompareSheet.root)
        addCompareDialog.show()

        addCompareSheet.apply {
            tvCountry.text = "Add Queries"
            tvQueries.text = "Add Query"
            btnAddQuery.setOnClickListener {
                val query = etNotes.text.toString().trim()
                if(query.isNotEmpty()){
                    keywords.add(query)
                    showShimmer(true)
                    addCompareDialog.dismiss()
                    fetchTrends()
                }
            }
            btnClose.setOnClickListener {
                addCompareDialog.dismiss()
            }
        }
    }

    fun editQuery(position: Int){
        addCompareSheet = SheetAddCompareBinding.inflate(layoutInflater)
        addCompareDialog = Dialog(this)
        addCompareDialog.setContentView(addCompareSheet.root)
        addCompareDialog.show()

        addCompareSheet.apply {
            tvCountry.text = "Edit Queries"
            tvQueries.text = "Edit Query"
            etNotes.setText(keywords[position])
            btnAddQuery.setOnClickListener {
                val query = etNotes.text.toString().trim()
                if(query.isNotEmpty()){
                    keywords[position] = query
                    showShimmer(true)
                    addCompareDialog.dismiss()
                    fetchTrends()
                }
            }
            btnClose.setOnClickListener {
                addCompareDialog.dismiss()
            }
        }
    }

    fun showRelatedDialog(data: CommonTrendModel) {
        relatedQueryBinding = SheetRelatedQueryBinding.inflate(layoutInflater)
        relatedDialog = Dialog(this)
        relatedDialog.setContentView(relatedQueryBinding.root)
        relatedDialog.show()
        relatedQueryBinding.tvQueries.text = data.name
        relatedQueryBinding.btnClose.setOnClickListener {
            relatedDialog.dismiss()
        }
    }

    private fun loadChart(keywords: List<String>, trends: TrendsData) {
        val colors = arrayOf(Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW, Color.BLACK)
        val interest = trends.interestOverTime
        val lineData = mutableListOf<LineDataSet>()
        for ((index, keyword) in keywords.withIndex()) {
            var i = 0
            val summary = ArrayList<Entry>()
            for ((_, value) in interest) {
                summary.add(Entry(i.toFloat(), value[index].toFloat()))
                i++
            }
            val summaryLineDataSet = LineDataSet(summary, keyword)
            summaryLineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
            summaryLineDataSet.color = colors[index]
            summaryLineDataSet.setDrawCircles(false)
            lineData.add(summaryLineDataSet)
        }
        val date = ArrayList<String>()
        for ((key, _) in interest) {
            date.add(key)
        }
        val legend = binding.ivDummyGraph.legend
        legend.isEnabled = true
        legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        legend.orientation = Legend.LegendOrientation.HORIZONTAL
        legend.setDrawInside(false)
        val tanggal = AxisDateFormatter(date.toArray(arrayOfNulls<String>(date.size)))
        binding.ivDummyGraph.description.isEnabled = false
        binding.ivDummyGraph.xAxis?.valueFormatter = tanggal
        binding.ivDummyGraph.xAxis.position = XAxis.XAxisPosition.BOTTOM
        binding.ivDummyGraph.data = LineData(lineData as List<ILineDataSet>?)
        binding.ivDummyGraph.animateXY(100, 500)
    }


    companion object {
        const val QUERY_DATA = "query_data"
    }
}