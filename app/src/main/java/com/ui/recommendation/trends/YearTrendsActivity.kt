package com.ui.recommendation.trends

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.recommendation.trends.TrendsYearDetailAdapter
import com.facebook.shimmer.ShimmerFrameLayout
import com.repo.response.TrendsYearResponse
import com.usahaq.usahaq.databinding.ActivityYearTrendsBinding
import com.util.ViewModelFactory
import com.viewmodel.TrendsViewModel

class YearTrendsActivity : AppCompatActivity() {

    private lateinit var binding : ActivityYearTrendsBinding
    private lateinit var viewModel : TrendsViewModel
    private lateinit var adapter : TrendsYearDetailAdapter
    private var trendsYearResponse : TrendsYearResponse ?=null
    private lateinit var shimmer : ShimmerFrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityYearTrendsBinding.inflate(layoutInflater)
        adapter = TrendsYearDetailAdapter()
        setContentView(binding.root)

        val factory = ViewModelFactory.getInstance(this.application)

        viewModel = ViewModelProvider(this, factory)[TrendsViewModel::class.java]

        shimmer = binding.shimmerLayout
        shimmer.visibility = View.VISIBLE
        binding.rvRelatedQueries.visibility = View.GONE
        shimmer.startShimmer()

        val year = intent.getStringExtra(YEAR)

        fetchYearTrendsData(year.toString())

        binding.apply {
            btnBack.setOnClickListener { onBackPressed() }
            rvRelatedQueries.adapter = adapter
            tvYear.text = year
            tvSeeWhatTrending.text = "See what trending in $year"
            rvRelatedQueries.layoutManager = LinearLayoutManager(this@YearTrendsActivity)
        }
    }


    private fun fetchYearTrendsData(year : String) {
        viewModel.getTrendsYear(year).observe(this, {result ->
            when{
                result.isUnauthorized -> {
                    Toast.makeText(this, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                result.isBadRequest -> {
                    Toast.makeText(this, "Bad request", Toast.LENGTH_SHORT).show()
                }
                result.isInternalError -> {
                    Toast.makeText(this, "Server error", Toast.LENGTH_SHORT).show()
                }
                result.isProcessingError -> {
                    Toast.makeText(this, "Error processing data", Toast.LENGTH_SHORT).show()
                }
                result.data != null -> {
                    trendsYearResponse = result.data
                    adapter.trendsYearList = trendsYearResponse!!.trendsYear
                    adapter.notifyDataSetChanged()
                    shimmer.stopShimmer()
                    shimmer.visibility = View.GONE
                    binding.rvRelatedQueries.visibility = View.VISIBLE
                }
            }
        })
    }

    companion object{
        const val YEAR = "year"
    }
}