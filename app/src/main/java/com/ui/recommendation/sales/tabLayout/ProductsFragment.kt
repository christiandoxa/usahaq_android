package com.ui.recommendation.sales.tabLayout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.recommendation.sales.SalesAdapter
import com.adapter.recommendation.sales.SalesItemDetailAdapter
import com.example.usahaq.model.CommonResponseModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.model.MachineLearningData
import com.repo.response.MachineLearningResponse
import com.ui.recommendation.sales.SalesActivity
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ActivitySalesBinding
import com.usahaq.usahaq.databinding.FragmentProductBinding
import com.usahaq.usahaq.databinding.SheetBinding
import com.util.DateHelper
import com.util.ViewModelFactory
import com.viewmodel.MachineLearningViewModel


class ProductsFragment(val activity : SalesActivity) : Fragment(), SalesAdapter.OnClickListener {

    private lateinit var binding : FragmentProductBinding
    private lateinit var adapter: SalesAdapter
    private lateinit var sheetDialog : BottomSheetDialog
    private lateinit var sheetBehaviour: BottomSheetBehavior<View>
    private lateinit var sheet: SheetBinding
    private lateinit var activityBinding : ActivitySalesBinding
    private lateinit var viewModel : MachineLearningViewModel
    private lateinit var shimmerFrameLayout : ShimmerFrameLayout
    private var totalResult = ArrayList<Float>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProductBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val factory = ViewModelFactory.getInstance(requireActivity().application)
        viewModel = ViewModelProvider(this, factory)[MachineLearningViewModel::class.java]
        adapter = SalesAdapter(this)
        binding.rvProduct.adapter = adapter
        binding.rvProduct.layoutManager = LinearLayoutManager(requireContext())
        sheet = SheetBinding.inflate(layoutInflater)
        activityBinding = ActivitySalesBinding.inflate(layoutInflater)
        sheetBehaviour = BottomSheetBehavior.from(activityBinding.bottomSheet)
        sheetDialog = BottomSheetDialog(requireContext())
        sheetDialog.setContentView(sheet.root)
        shimmerFrameLayout = binding.shimmerLayout
        shimmerFrameLayout.visibility = View.VISIBLE
        shimmerFrameLayout.startShimmer()
        fetchAllData()
    }

    private fun showBottomSheetDialog(machineLearningData: MachineLearningData) {
        if (sheetBehaviour.state == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviour.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        sheet.btnClose.setOnClickListener {
            sheetDialog.dismiss()
        }
        sheetDialog.window?.statusBarColor = 0x04000000
        sheet.detail.text = DateHelper.getCurrentDate()
        sheet.tvRion.text = machineLearningData.name
        sheet.tvPrice.text = getString(R.string.format_amount, machineLearningData.result[0])
        val result = machineLearningData.result.toMutableList() as ArrayList
        result.removeAt(0)
        val predictionAdapter = SalesItemDetailAdapter()
        predictionAdapter.machineLearningDataList = result
        sheet.rvDate.adapter = predictionAdapter
        sheet.rvDate.layoutManager = LinearLayoutManager(requireContext())
        sheetDialog.show()
    }

    private fun fetchAllData() {
        fetchData("chicken_sales")
    }

    private fun fetchData(modelType: String) {
        viewModel.getPrediction(modelType).observe(viewLifecycleOwner, { result ->
            processResponseData(result, modelType)
            shimmerFrameLayout.stopShimmer()
            shimmerFrameLayout.visibility = View.GONE
        })
    }

    private fun processResponseData(
        result: CommonResponseModel<MachineLearningResponse>,
        modelType: String
    ) {
        when {
            result.isUnauthorized -> {
                Toast.makeText(context, "Unauthorized", Toast.LENGTH_SHORT).show()
            }
            result.isBadRequest -> {
                Toast.makeText(context, "Bad request", Toast.LENGTH_SHORT).show()
            }
            result.isInternalError -> {
                Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
            }
            result.isProcessingError -> {
                Toast.makeText(context, "Error processing data", Toast.LENGTH_SHORT).show()
            }
            result.data != null -> {
                adapter.machineLearningDataList.add(
                    MachineLearningData(
                        modelType,
                        result.data!!.result
                    )
                )
                activity.setPrediction(getString(R.string.format_amount,adapter.machineLearningDataList[0].result[0]))
                totalResult = adapter.machineLearningDataList[0].result
                activity.setData(totalResult)
                adapter.notifyDataSetChanged()
            }
        }
    }



    override fun onClick(machineLearningData: MachineLearningData) {
        showBottomSheetDialog(machineLearningData)
    }
}