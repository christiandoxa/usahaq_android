package com.ui.recommendation.sales.tabLayout

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.FragmentPredictionTrendingBinding

class PredictionTrendingFragment : Fragment() {

    private lateinit var binding : FragmentPredictionTrendingBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPredictionTrendingBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}