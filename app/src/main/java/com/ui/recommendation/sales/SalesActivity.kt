package com.ui.recommendation.sales

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.usahaq.model.CommonResponseModel
import com.google.android.material.tabs.TabLayoutMediator
import com.model.MachineLearningData
import com.repo.response.MachineLearningResponse
import com.ui.recommendation.price.PriceRecommendationAdapter
import com.usahaq.usahaq.databinding.ActivitySalesBinding
import com.util.DateHelper

class SalesActivity : AppCompatActivity() {

    private lateinit var binding : ActivitySalesBinding
    private lateinit var adapter : SalesViewPagerAdapter
    private var totalResult = ArrayList<Float>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySalesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapter = SalesViewPagerAdapter(this@SalesActivity, this)
        binding.viewpagerDetail.adapter = adapter
        binding.viewpagerDetail.offscreenPageLimit = adapter.itemCount
        TabLayoutMediator(
            binding.tableLayoutDetail,
            binding.viewpagerDetail
        ) { tab, position ->
            when (position) {
                0 -> tab.text = "Products"
                1 -> tab.text = "Trending"
            }
        }.attach()
        binding.btnBack.setOnClickListener { onBackPressed() }

        binding.apply {
            tvDate.text = "${DateHelper.getDayName(0)}, ${DateHelper.getCurrentDate()}"
            clSalesPrediction.setOnClickListener {
                val intent = Intent(this@SalesActivity, DetailSalesActivity::class.java)
                intent.putExtra(DetailSalesActivity.DATA, totalResult)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        }
    }

    fun setPrediction(prediction : String){
        binding.tvCurrentPredict.text = prediction
    }

    fun setData(data : ArrayList<Float>){
        totalResult = data
    }



}