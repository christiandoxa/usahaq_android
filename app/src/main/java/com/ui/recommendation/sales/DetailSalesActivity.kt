package com.ui.recommendation.sales

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.recommendation.sales.DetailSalesAdapter
import com.usahaq.usahaq.databinding.ActivityDetailSalesBinding
import com.util.DateHelper

class DetailSalesActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDetailSalesBinding
    private lateinit var adapter : DetailSalesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailSalesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val data =  intent.getSerializableExtra(DATA) as ArrayList<Float>
        adapter = DetailSalesAdapter()



        binding.apply {
            tvDate.text = "${DateHelper.getDayName(0)}, ${DateHelper.getCurrentDate()}"
            tvCurrentPredict.text = data[0].toString()
            data.removeAt(0)
            adapter.listDetail.addAll(data)
            rvTrendsToday.adapter = adapter
            rvTrendsToday.layoutManager = LinearLayoutManager(this@DetailSalesActivity)
            btnBack.setOnClickListener { onBackPressed() }
        }
    }

    companion object{
        const val DATA = "data"
    }
}