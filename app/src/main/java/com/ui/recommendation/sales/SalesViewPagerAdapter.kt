package com.ui.recommendation.sales

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ui.recommendation.price.tabLayout.ForYouFragment
import com.ui.recommendation.price.tabLayout.MarketFragment
import com.ui.recommendation.price.tabLayout.TrendingFragment
import com.ui.recommendation.sales.tabLayout.PredictionTrendingFragment
import com.ui.recommendation.sales.tabLayout.ProductsFragment

class SalesViewPagerAdapter(activity : AppCompatActivity, val salesActivity: SalesActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ProductsFragment(salesActivity)
            1 -> PredictionTrendingFragment()
            else -> ForYouFragment()
        }
    }
}