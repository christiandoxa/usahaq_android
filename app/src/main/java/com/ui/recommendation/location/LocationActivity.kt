package com.ui.recommendation.location

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import com.adapter.recommendation.location.LocationDetailAdapter
import com.usahaq.usahaq.databinding.ActivityLocationBinding

class LocationActivity : AppCompatActivity() {

    private lateinit var binding : ActivityLocationBinding
    private lateinit var adapter : LocationDetailAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLocationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapter = LocationDetailAdapter()
        adapter.setData()

        binding.apply {
            rvPopularCity.adapter = adapter
            rvPopularCity.layoutManager = GridLayoutManager(this@LocationActivity, 2)
            btnBack.setOnClickListener { onBackPressed() }
        }
    }
}