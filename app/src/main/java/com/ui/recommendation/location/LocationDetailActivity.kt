package com.ui.recommendation.location

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.recommendation.location.NearbyAdapter
import com.bumptech.glide.Glide
import com.model.LocationModel
import com.usahaq.usahaq.databinding.ActivityLocationDetailBinding

class LocationDetailActivity : AppCompatActivity() {

    private lateinit var binding : ActivityLocationDetailBinding
    private lateinit var adapter : NearbyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLocationDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val city = intent.getParcelableExtra<LocationModel>(CITY)

        adapter = NearbyAdapter()
        adapter.setData()

        binding.apply {
            tvDetailLocation.text = city?.city
            tvPin.text = city?.city
            Glide.with(this@LocationDetailActivity).load(city?.cityImage).into(ivClipboard)
            rvNearby.adapter = adapter
            rvNearby.layoutManager = LinearLayoutManager(this@LocationDetailActivity)
            btnBack.setOnClickListener { onBackPressed() }
        }
    }

    companion object{
        const val CITY = "city"
    }
}