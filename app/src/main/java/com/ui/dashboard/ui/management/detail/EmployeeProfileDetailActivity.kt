package com.ui.dashboard.ui.management.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.model.EmployeeDataClass
import com.usahaq.usahaq.databinding.ActivityEmployeeProfileDetailBinding

class EmployeeProfileDetailActivity : AppCompatActivity() {

    private lateinit var binding : ActivityEmployeeProfileDetailBinding
    private lateinit var employee : EmployeeDataClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmployeeProfileDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        employee = intent.getParcelableExtra<EmployeeDataClass>(EMPLOYEE) as EmployeeDataClass

        binding.btnBack.setOnClickListener { onBackPressed() }

        loadData()
    }

    private fun loadData(){
        binding.apply {
            tvName.text = employee.name
            tvPerformancePercentage.text = employee.performance.toString()
            tvPosition.text = employee.position
            tvAttendancePercentage.text = employee.attendance.toString()
            Glide.with(root)
                .load(employee.employeeImage)
                .into(ivPoster)
        }
    }

    companion object{
        const val EMPLOYEE = "employee"
    }
}