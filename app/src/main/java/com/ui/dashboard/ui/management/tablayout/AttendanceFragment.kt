package com.ui.dashboard.ui.management.tablayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.management.EmployeeAttandanceAdapter
import com.usahaq.usahaq.databinding.FragmentAttendanceBinding

class AttendanceFragment : Fragment() {

    private lateinit var binding : FragmentAttendanceBinding
    private lateinit var adapter : EmployeeAttandanceAdapter


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAttendanceBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = EmployeeAttandanceAdapter()
        adapter.setData()
        binding.rvPresent.layoutManager = LinearLayoutManager(requireContext())
        binding.rvPresent.adapter = adapter
    }
}