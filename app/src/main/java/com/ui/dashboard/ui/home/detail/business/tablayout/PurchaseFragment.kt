package com.ui.dashboard.ui.home.detail.business.tablayout

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.detailbusiness.TransactionAdapter
import com.ui.dashboard.ui.home.transaction.TransactionNoteActivity
import com.usahaq.usahaq.databinding.FragmentPurchaseBinding
import com.util.ViewModelFactory
import com.viewmodel.TransactionViewModel

class PurchaseFragment : Fragment() {

    private lateinit var binding: FragmentPurchaseBinding
    private lateinit var adapter: TransactionAdapter
    private lateinit var viewModel: TransactionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPurchaseBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = TransactionAdapter()
        binding.rvPurchase.adapter = adapter
        binding.rvPurchase.layoutManager = LinearLayoutManager(context)
        val factory = ViewModelFactory.getInstance(requireActivity().application)
        viewModel = ViewModelProvider(this, factory)[TransactionViewModel::class.java]
        viewModel.getAllTransaction(false).observe(viewLifecycleOwner, { transactionResponse ->
            when {
                transactionResponse.isUnauthorized -> {
                    Toast.makeText(context, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                transactionResponse.isBadRequest -> {
                    Toast.makeText(context, "Bad request", Toast.LENGTH_SHORT).show()
                }
                transactionResponse.isInternalError -> {
                    Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
                }
                transactionResponse.isProcessingError -> {
                    Toast.makeText(context, "Error processing data", Toast.LENGTH_SHORT).show()
                }
                transactionResponse.data != null -> {
                    adapter.purchaseData = transactionResponse.data!!.transactions
                    adapter.notifyDataSetChanged()
                }
            }
        })
    }
}