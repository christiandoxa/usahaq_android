package com.ui.dashboard.ui.home.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.usahaq.usahaq.databinding.FragmentRunOutBinding

class RunOutFragment : Fragment() {

    private lateinit var binding : FragmentRunOutBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRunOutBinding.inflate(layoutInflater)
        return binding.root
    }

}