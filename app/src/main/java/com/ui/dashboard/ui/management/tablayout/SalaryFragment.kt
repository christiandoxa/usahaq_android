package com.ui.dashboard.ui.management.tablayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.usahaq.usahaq.databinding.FragmentSalaryBinding

class SalaryFragment : Fragment() {

    private lateinit var binding : FragmentSalaryBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSalaryBinding.inflate(layoutInflater)
        return binding.root
    }
}