package com.ui.dashboard.ui.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.github.dhaval2404.imagepicker.ImagePicker
import com.repo.request.ProductRequest
import com.ui.dashboard.DashboardActivity
import com.usahaq.usahaq.databinding.ActivityAddProductBinding
import com.util.ViewModelFactory
import com.viewmodel.ProductViewModel
import java.io.File

class AddProductActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddProductBinding
    private lateinit var viewModel: ProductViewModel
    private var file: File? = null
    private val productRequest = ProductRequest()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddProductBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val factory = ViewModelFactory.getInstance(application)
        viewModel = ViewModelProvider(this, factory)[ProductViewModel::class.java]
        binding.apply {
            btnAddProduct.setOnClickListener {
                var isEmptyField = false
                if (etProduct.text.toString().trim().isEmpty()) {
                    isEmptyField = true
                    etProduct.error = "This field can't be empty"
                }
                if (etSell.text.toString().trim().isEmpty()) {
                    isEmptyField = true
                    etSell.error = "This field can't be empty"
                }
                if (etStocks.text.toString().trim().isEmpty()) {
                    isEmptyField = true
                    etStocks.error = "This field can't be empty"
                }
                if (!isEmptyField) create()
            }
            profileProduct.setOnClickListener {
                pickImage()
            }
            tvAddPhoto.setOnClickListener {
                pickImage()
            }
            btnBack.setOnClickListener {
                onBackPressed()
            }
        }
    }

    private fun pickImage() {
        ImagePicker.with(this)
            .compress(1024) //Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)
            .cropSquare() //Crop square image, its same as crop(1f, 1f)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT****", "OK")
            if (requestCode == ImagePicker.REQUEST_CODE) {
                val fileUri = data.data
                binding.profileProduct.setImageURI(fileUri)
                file = ImagePicker.getFile(data)!!
            }
        }
    }

    private fun create() {
        val idBusiness = intent.getIntExtra(ID_BUSINESS_DATA, 0)
        productRequest.idBusiness = idBusiness
        binding.apply {
            productRequest.apply {
                productName = etProduct.text.toString()
                price = etSell.text.toString().toInt()
                stock = etStocks.text.toString().toInt()
                catalog = switchButton.isChecked
                image = file
            }
        }
        viewModel.createProduct(productRequest).observe(this, { productResponse ->
            when {
                productResponse.isBadRequest -> {
                    Toast.makeText(this, "Bad request", Toast.LENGTH_SHORT).show()
                }
                productResponse.isUnauthorized -> {
                    Toast.makeText(this, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                productResponse.isConflict -> {
                    Toast.makeText(this, "Conflict", Toast.LENGTH_SHORT).show()
                }
                productResponse.isInternalError -> {
                    Toast.makeText(this, "Server error", Toast.LENGTH_SHORT).show()
                }
                productResponse.isProcessingError -> {
                    Toast.makeText(this, "Processing error", Toast.LENGTH_SHORT).show()
                }
                productResponse.data != null -> {
                    HomeFragment.CREATED = true
                    val intent = Intent(
                        this,
                        DashboardActivity::class.java
                    )
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                }
            }
        })
    }

    companion object {
        const val ID_BUSINESS_DATA = "ID_BUSINESS_DATA"
    }
}