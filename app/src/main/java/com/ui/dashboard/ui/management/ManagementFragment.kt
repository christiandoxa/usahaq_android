package com.ui.dashboard.ui.management

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.usahaq.usahaq.databinding.FragmentManagementBinding

class ManagementFragment : Fragment() {

    private lateinit var binding : FragmentManagementBinding
    private lateinit var adapter: ManagementAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentManagementBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ManagementAdapter(this)

        binding.viewpagerDetail.adapter = adapter
        binding.viewpagerDetail.offscreenPageLimit = adapter.itemCount

        TabLayoutMediator(
            binding.tableLayoutDetail,
            binding.viewpagerDetail
        ) { tab, position ->
            when(position){
                0-> tab.text = "Attendance"
                1-> tab.text = "Salary"
            }
        }.attach()
    }
}