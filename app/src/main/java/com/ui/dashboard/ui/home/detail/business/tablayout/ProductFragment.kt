package com.ui.dashboard.ui.home.detail.business.tablayout

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.detailbusiness.ProductAdapter
import com.repo.response.BusinessData
import com.repo.response.MultipleProductResponse
import com.usahaq.usahaq.databinding.FragmentProductBinding
import com.util.ViewModelFactory
import com.viewmodel.ProductViewModel

class ProductFragment(
    private val businessData: BusinessData,
    val productListener: ProductListener
) : Fragment() {

    private lateinit var binding: FragmentProductBinding
    private lateinit var viewModel: ProductViewModel
    private lateinit var adapter: ProductAdapter
    private var multipleProductResponse: MultipleProductResponse? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentProductBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ProductAdapter()
        val factory = ViewModelFactory.getInstance(requireActivity().application)
        viewModel = ViewModelProvider(this, factory)[ProductViewModel::class.java]

        binding.apply {
            rvProduct.adapter = adapter
            rvProduct.layoutManager = LinearLayoutManager(requireContext())
        }

        fetchProduct()
    }

    private fun fetchProduct() {
        viewModel.getAllProduct(businessData.id.toString()).observe(viewLifecycleOwner, { productResponse ->
            when {
                productResponse.isUnauthorized -> {
                    Toast.makeText(requireContext(), "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                productResponse.isBadRequest -> {
                    Toast.makeText(requireContext(), "Bad request", Toast.LENGTH_SHORT).show()
                }
                productResponse.isInternalError -> {
                    Toast.makeText(requireContext(), "Server error", Toast.LENGTH_SHORT).show()
                }
                productResponse.isProcessingError -> {
                    Toast.makeText(requireContext(), "Error processing data", Toast.LENGTH_SHORT).show()
                }
                productResponse.data != null -> {
                    multipleProductResponse = productResponse.data
                    productListener.onProductFetched(multipleProductResponse!!)
                    adapter.productData = multipleProductResponse!!.products
                    adapter.notifyDataSetChanged()
                }
            }
        })
    }

    interface ProductListener {
        fun onProductFetched(multipleProductResponse: MultipleProductResponse)
    }
}