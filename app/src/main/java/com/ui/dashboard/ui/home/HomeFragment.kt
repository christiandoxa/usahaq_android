package com.ui.dashboard.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.detailbusiness.BusinessAdapter
import com.facebook.shimmer.ShimmerFrameLayout
import com.ui.recommendation.location.LocationActivity
import com.ui.recommendation.price.PriceRecommendationActivity
import com.ui.recommendation.sales.SalesActivity
import com.ui.recommendation.trends.Trends
import com.usahaq.usahaq.databinding.FragmentHomeBinding
import com.util.ViewModelFactory
import com.viewmodel.BusinessViewModel

class HomeFragment : Fragment() {

    private lateinit var binding: FragmentHomeBinding
    private lateinit var businessAdapter: BusinessAdapter
    private lateinit var businessViewModel: BusinessViewModel
    private lateinit var shimmerFrameLayout : ShimmerFrameLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val factory = ViewModelFactory.getInstance(requireActivity().application)
        businessViewModel = ViewModelProvider(this, factory)[BusinessViewModel::class.java]
        val linearLayoutManager = LinearLayoutManager(context)
        shimmerFrameLayout = binding.shimmerLayout
        shimmerFrameLayout.visibility = View.VISIBLE
        binding.rvBusiness.visibility = View.GONE
        shimmerFrameLayout.startShimmer()
        businessAdapter = BusinessAdapter()
        fetchBusiness()
        binding.apply {
            rvBusiness.adapter = businessAdapter
            rvBusiness.layoutManager = linearLayoutManager
            btnCreateBusiness.setOnClickListener {
                startActivity(Intent(requireContext(), CreateBusinessActivity::class.java))
            }
            btnAddBusiness.setOnClickListener {
                startActivity(Intent(requireContext(), CreateBusinessActivity::class.java))
            }

            cvProduct.setOnClickListener {
                startActivity(Intent(requireContext(), Trends::class.java))
            }

            cvLocation.setOnClickListener {
                startActivity(Intent(requireContext(), LocationActivity::class.java))
            }

            cvSales.setOnClickListener {
                startActivity(Intent(requireContext(), SalesActivity::class.java))
            }

            cvPrice.setOnClickListener {
                startActivity(Intent(requireContext(), PriceRecommendationActivity::class.java))
            }
        }
    }

    private fun fetchBusiness() {
        businessViewModel.getAllBusiness().observe(viewLifecycleOwner, { multipleBusinessResponse ->
            when {
                multipleBusinessResponse.isUnauthorized -> {
                    Toast.makeText(context, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                multipleBusinessResponse.isBadRequest -> {
                    Toast.makeText(context, "Bad request", Toast.LENGTH_SHORT).show()
                }
                multipleBusinessResponse.isInternalError -> {
                    Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
                }
                multipleBusinessResponse.isProcessingError -> {
                    Toast.makeText(context, "Error processing data", Toast.LENGTH_SHORT).show()
                }
                multipleBusinessResponse.data ==null && multipleBusinessResponse.data!!.businesses.size <=0 ->{
                    binding.clCreateBusiness.visibility = View.VISIBLE
                    binding.clBusinessCreated.visibility = View.GONE
                }
                multipleBusinessResponse.data != null && multipleBusinessResponse.data!!.businesses.size > 0 -> {
                    binding.clCreateBusiness.visibility = View.GONE
                    binding.clBusinessCreated.visibility = View.VISIBLE
                    shimmerFrameLayout.visibility = View.GONE
                    shimmerFrameLayout.stopShimmer()
                    binding.rvBusiness.visibility = View.VISIBLE
                    businessAdapter.businessData = multipleBusinessResponse.data!!.businesses
                    businessAdapter.notifyDataSetChanged()
                }
            }
        })
    }

    companion object {
        var CREATED: Boolean? = null
    }
}