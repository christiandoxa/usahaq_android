package com.ui.dashboard.ui.home.transaction

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.usahaq.usahaq.databinding.ActivityTransactionSuccesBinding

class TransactionSuccesActivity : AppCompatActivity() {

    private lateinit var binding : ActivityTransactionSuccesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransactionSuccesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnBack.setOnClickListener {
            startActivity(
                Intent(
                    this@TransactionSuccesActivity,
                    AddTransactionActivity::class.java
                )
            )
            finish()
        }
    }


}