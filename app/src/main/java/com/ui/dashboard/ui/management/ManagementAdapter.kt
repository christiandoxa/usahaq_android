package com.ui.dashboard.ui.management

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.ui.dashboard.ui.management.tablayout.AttendanceFragment
import com.ui.dashboard.ui.management.tablayout.SalaryFragment

class ManagementAdapter (private val fragment : ManagementFragment ) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> AttendanceFragment()
            1 -> SalaryFragment()
            else -> AttendanceFragment()
        }
    }
}