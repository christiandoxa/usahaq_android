package com.ui.dashboard.ui.home.detail.business

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.detailbusiness.SummaryAdapter
import com.model.SummaryTitleModel
import com.usahaq.usahaq.databinding.ActivitySalesSummaryDetailBinding

class SalesSummaryDetail : AppCompatActivity() {

    private lateinit var binding : ActivitySalesSummaryDetailBinding
    private lateinit var adapter : SummaryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySalesSummaryDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        adapter = SummaryAdapter()
        adapter.setData()

        val summary = intent.getParcelableExtra<SummaryTitleModel>(SUMMARY)

        binding.apply {
            btnBack.setOnClickListener { onBackPressed() }
            rvSummary.adapter = adapter
            rvSummary.layoutManager = LinearLayoutManager(this@SalesSummaryDetail)
            tvTitle.text = summary?.titlePage
            tvTitleDetail.text = summary?.titleDetails
            tvTitleDaily.text = summary?.titleDaily
        }
    }

    companion object{
        const val SUMMARY = "summary"
    }
}