package com.ui.dashboard.ui.home.detail.business.tablayout

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.detailbusiness.TransactionAdapter
import com.usahaq.usahaq.databinding.FragmentSalesBinding
import com.util.ViewModelFactory
import com.viewmodel.TransactionViewModel

class SalesFragment : Fragment() {

    private lateinit var binding: FragmentSalesBinding
    private lateinit var adapter: TransactionAdapter
    private lateinit var viewModel: TransactionViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSalesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = TransactionAdapter()
        binding.apply {
            rvSales.adapter = adapter
            rvSales.layoutManager = LinearLayoutManager(requireContext())
        }
        val factory = ViewModelFactory.getInstance(requireActivity().application)
        viewModel = ViewModelProvider(this, factory)[TransactionViewModel::class.java]
        viewModel.getAllTransaction(true).observe(viewLifecycleOwner, { transactionResponse ->
            when {
                transactionResponse.isUnauthorized -> {
                    Toast.makeText(context, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                transactionResponse.isBadRequest -> {
                    Toast.makeText(context, "Bad request", Toast.LENGTH_SHORT).show()
                }
                transactionResponse.isInternalError -> {
                    Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
                }
                transactionResponse.isProcessingError -> {
                    Toast.makeText(context, "Error processing data", Toast.LENGTH_SHORT).show()
                }
                transactionResponse.data != null -> {
                    adapter.purchaseData = transactionResponse.data!!.transactions
                    adapter.notifyDataSetChanged()
                }
            }
        })
    }
}