package com.ui.dashboard.ui.home.detail.business

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.model.SummaryTitleModel
import com.usahaq.usahaq.databinding.ActivitySalesSummaryBinding
import com.util.AxisDateFormatter

class SalesSummary : AppCompatActivity() {

    private lateinit var binding : ActivitySalesSummaryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySalesSummaryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadChart()

        binding.apply {
            btnBack.setOnClickListener { onBackPressed() }

            cvCogs.setOnClickListener {
                detailSummary(SummaryTitleModel("COGS", "Cost of Good Sales Details"
                , "Daily COGS"))
            }

            cvNet.setOnClickListener {
                detailSummary(SummaryTitleModel("Net Sales", "Net Sales Details",
                "Daily Sales"))
            }

            cvGrossProfit.setOnClickListener {
                detailSummary(
                    SummaryTitleModel("Gross Profit", "Gross Profit Details",
                "Daily Profit")
                )
            }

            cvAverage.setOnClickListener {
                detailSummary(SummaryTitleModel("Average Sales", "Average Sales per Transaction",
                "Daily Averages"))
            }

            cvTransaction.setOnClickListener {
                detailSummary(SummaryTitleModel("Transaction", "Transaction Detail",
                "Daily Transaction"))
            }
        }
    }

    private fun detailSummary(summaryTitleModel: SummaryTitleModel){
        val intent = Intent(this@SalesSummary, SalesSummaryDetail::class.java)
        intent.putExtra(SalesSummaryDetail.SUMMARY, summaryTitleModel)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    private fun loadChart(){
        val summary = ArrayList<Entry>()
        summary.add(Entry(0F, 149000F))
        summary.add(Entry(1F, 113000F))
        summary.add(Entry(2F, 196000F))
        summary.add(Entry(3F, 106000F))
        summary.add(Entry(4F, 181000F))
        summary.add(Entry(5F, 218000F))
        summary.add(Entry(6F, 247000F))

        val summary2 = ArrayList<Entry>()
        summary2.add(Entry(0F, 150000F))
        summary2.add(Entry(1F, 181000F))
        summary2.add(Entry(2F, 175000F))
        summary2.add(Entry(3F, 162000F))
        summary2.add(Entry(4F, 187000F))
        summary2.add(Entry(5F, 193000F))
        summary2.add(Entry(6F, 221000F))

        val date = ArrayList<String>()
        date.add("01-Apr")
        date.add("02-Apr")
        date.add("03-Apr")
        date.add("04-Apr")
        date.add("05-Apr")
        date.add("06-Apr")
        date.add("07-Apr")


        val summaryLineDataSet = LineDataSet(summary, "Income")
        summaryLineDataSet.mode = LineDataSet.Mode.CUBIC_BEZIER
        summaryLineDataSet.color = Color.BLUE
        summaryLineDataSet.circleRadius = 5f
        summaryLineDataSet.setCircleColor(Color.BLUE)

        val summaryLineDataSet2 = LineDataSet(summary2, "Outcome")
        summaryLineDataSet2.mode = LineDataSet.Mode.CUBIC_BEZIER
        summaryLineDataSet2.color = Color.RED
        summaryLineDataSet2.circleRadius = 5f
        summaryLineDataSet2.setCircleColor(Color.RED)

        val legend = binding.ivChart.legend
        legend.isEnabled = true
        legend.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        legend.orientation = Legend.LegendOrientation.HORIZONTAL
        legend.setDrawInside(false)

        val tanggal = AxisDateFormatter(date.toArray(arrayOfNulls<String>(date.size)))

        binding.ivChart.description.isEnabled = false
        binding.ivChart.xAxis?.valueFormatter = tanggal
        binding.ivChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        binding.ivChart.data = LineData(summaryLineDataSet, summaryLineDataSet2)
        binding.ivChart.animateXY(100, 500)
    }
}