package com.ui.dashboard.ui.home.detail.product

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.repo.response.ProductData
import com.ui.dashboard.ui.home.AddProductActivity
import com.usahaq.usahaq.databinding.ActivityDetailProductBinding
import com.usahaq.usahaq.databinding.SheetDeleteProductBinding

class DetailProductActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDetailProductBinding
    private lateinit var sheetBehaviour: BottomSheetBehavior<View>
    private lateinit var sheet: SheetDeleteProductBinding
    private lateinit var sheetDialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailProductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val productData = intent.getParcelableExtra<ProductData>(PRODUCT)

        sheet = SheetDeleteProductBinding.inflate(layoutInflater)
        sheetBehaviour = BottomSheetBehavior.from(binding.bottomSheet)
        sheetDialog = BottomSheetDialog(this)
        sheetDialog.setContentView(sheet.root)

        loadData(productData!!)

        binding.apply {
            btnBack.setOnClickListener { onBackPressed() }

            btnEdit.setOnClickListener {
                startActivity(Intent(this@DetailProductActivity, AddProductActivity::class.java))
            }

            btnDelete.setOnClickListener {
                deleteProduct()
            }
        }
    }

    private fun loadData(productData: ProductData){
        binding.apply {
            Glide.with(this@DetailProductActivity).load(productData.productImage)
                .into(profileProduct)
            tvProductName.text = productData.productName
            tvPriceAmount.text = productData.price.toString()
            tvStocksAmount.text = productData.stock.toString()
        }
    }

    private fun deleteProduct(){
        if (sheetBehaviour.state == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviour.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        sheet.btnClose.setOnClickListener {
            sheetDialog.dismiss()
        }
        sheetDialog.window?.statusBarColor = 0x04000000
        sheetDialog.show()
    }

    companion object{
        const val PRODUCT = "product"
    }
}