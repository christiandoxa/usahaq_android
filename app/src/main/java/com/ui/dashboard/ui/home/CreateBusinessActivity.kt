package com.ui.dashboard.ui.home

import android.app.Activity
import android.content.Intent
import android.location.Address
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.github.dhaval2404.imagepicker.ImagePicker
import com.repo.request.BusinessRequest
import com.schibstedspain.leku.*
import com.ui.dashboard.DashboardActivity
import com.usahaq.usahaq.databinding.ActivityCreateBusinessBinding
import com.util.ViewModelFactory
import com.viewmodel.BusinessViewModel
import java.io.File

private const val MAP_PICKER_REQUEST_CODE: Int = 1

class CreateBusinessActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCreateBusinessBinding
    private lateinit var viewModel: BusinessViewModel
    private var file: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCreateBusinessBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val factory = ViewModelFactory.getInstance(application)
        viewModel = ViewModelProvider(this, factory)[BusinessViewModel::class.java]
        binding.apply {
            btnAddBusiness.setOnClickListener {
                var isEmptyField = false

                if (etAddress.text.toString().trim().isEmpty()) {
                    isEmptyField = true
                    etAddress.error = "This Field can't be empty"
                }
                if (etLocation.text.toString().trim().isEmpty()) {
                    isEmptyField = true
                    etLocation.error = "This Field can't be empty"
                }
                if (etBusiness.text.toString().trim().isEmpty()) {
                    isEmptyField = true
                    etBusiness.error = "This Field can't be empty"
                }

                if (!isEmptyField) {
                    createBusiness()
                }

            }
            etLocation.setOnClickListener {
                val locationPickerIntent = LocationPickerActivity.Builder()
                    .withSearchZone("id_ID")
                    .withSatelliteViewHidden()
                    .build(applicationContext)
                startActivityForResult(locationPickerIntent, MAP_PICKER_REQUEST_CODE)
            }
            profileBusiness.setOnClickListener { pickImage() }
            tvAddPhoto.setOnClickListener { pickImage() }
            btnBack.setOnClickListener { onBackPressed() }
        }
    }

    private fun createBusiness() {
        val request = BusinessRequest(
            binding.etBusiness.text.toString(),
            binding.etAddress.text.toString(),
            binding.etLocation.text.toString(),
            file
        )
        viewModel.createBusiness(request).observe(this, { commonResponse ->
            when {
                commonResponse.isBadRequest -> {
                    Toast.makeText(this, "Bad request", Toast.LENGTH_SHORT).show()
                }
                commonResponse.isUnauthorized -> {
                    Toast.makeText(this, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                commonResponse.isConflict -> {
                    Toast.makeText(this, "Conflict", Toast.LENGTH_SHORT).show()
                }
                commonResponse.isInternalError -> {
                    Toast.makeText(this, "Server error", Toast.LENGTH_SHORT).show()
                }
                commonResponse.isProcessingError -> {
                    Toast.makeText(this, "Processing error", Toast.LENGTH_SHORT).show()
                }
                commonResponse.data != null -> {
                    HomeFragment.CREATED = true
                    val intent = Intent(
                        this@CreateBusinessActivity,
                        DashboardActivity::class.java
                    )
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                    finish()
                }
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            Log.d("RESULT****", "OK")
            if (requestCode == MAP_PICKER_REQUEST_CODE) {
                val latitude = data.getDoubleExtra(LATITUDE, 0.0)
                Log.d("LATITUDE****", latitude.toString())
                val longitude = data.getDoubleExtra(LONGITUDE, 0.0)
                Log.d("LONGITUDE****", longitude.toString())
                val address = data.getStringExtra(LOCATION_ADDRESS)
                Log.d("ADDRESS****", address.toString())
                val postalcode = data.getStringExtra(ZIPCODE)
                Log.d("POSTALCODE****", postalcode.toString())
                val fullAddress = data.getParcelableExtra<Address>(ADDRESS)
                if (fullAddress != null) {
                    Log.d("FULL ADDRESS****", fullAddress.toString())
                }
                val latLng = "$latitude,$longitude"
                binding.etLocation.setText(latLng)
                binding.etLocation.isEnabled = false
                binding.etAddress.setText(address)
            } else if (requestCode == ImagePicker.REQUEST_CODE) {
                val fileUri = data.data
                binding.profileBusiness.setImageURI(fileUri)
                file = ImagePicker.getFile(data)!!
            }
        }
    }

    private fun pickImage() {
        ImagePicker.with(this)
            .compress(1024) //Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)
            .cropSquare() //Crop square image, its same as crop(1f, 1f)
            .start()
    }
}
