package com.ui.dashboard.ui.home.transaction

import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.model.Transaction
import com.repo.request.TransactionRequest
import com.repo.response.PaymentResponse
import com.usahaq.usahaq.databinding.ActivityTransactionSummaryBinding
import com.util.ViewModelFactory
import com.viewmodel.TransactionViewModel

class TransactionSummaryActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTransactionSummaryBinding
    private lateinit var viewModel: TransactionViewModel
    private var transaction: Transaction? = null
    private var paymentResponse: PaymentResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransactionSummaryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        transaction = intent.getParcelableExtra(TRANSACTION_DATA)
        val factory = ViewModelFactory.getInstance(application)
        viewModel = ViewModelProvider(this, factory)[TransactionViewModel::class.java]
        binding.btnConfirmation.setOnClickListener {
            createTransaction()
        }
        binding.btnBack.setOnClickListener { onBackPressed() }
        fetchSpinner()
    }

    private fun fetchSpinner() {
        viewModel.getAllPaymentMethod().observe(this, { paymentResponse ->
            when {
                paymentResponse.isUnauthorized -> {
                    Toast.makeText(applicationContext, "Unauthorized", Toast.LENGTH_SHORT)
                        .show()
                }
                paymentResponse.isBadRequest -> {
                    Toast.makeText(applicationContext, "Bad request", Toast.LENGTH_SHORT)
                        .show()
                }
                paymentResponse.isInternalError -> {
                    Toast.makeText(applicationContext, "Server error", Toast.LENGTH_SHORT)
                        .show()
                }
                paymentResponse.isProcessingError -> {
                    Toast.makeText(
                        applicationContext,
                        "Error processing data",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                paymentResponse.data != null -> {
                    this.paymentResponse = paymentResponse.data
                    val newStringList = ArrayList<String>()
                    for (paymentData in paymentResponse.data!!.paymentMethod) {
                        newStringList.add(paymentData.paymentMethod)
                    }
                    binding.apply {
                        val newAdapter = ArrayAdapter(
                            this@TransactionSummaryActivity,
                            android.R.layout.simple_spinner_item,
                            newStringList
                        )
                        newAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spinnerPaymentMethod.adapter = newAdapter
                    }
                }
            }
        })
    }

    private fun createTransaction() {
        if (transaction != null) {
            val idPaymentMethod =
                paymentResponse!!.paymentMethod.first { paymentData -> binding.spinnerPaymentMethod.selectedItem.toString() == paymentData.paymentMethod }.idPaymentMethod
            transaction!!.idPaymentMethod = idPaymentMethod.toString()
            viewModel.createTransaction(TransactionRequest(transaction!!))
                .observe(this, { transactionResponse ->
                    when {
                        transactionResponse.isUnauthorized -> {
                            Toast.makeText(applicationContext, "Unauthorized", Toast.LENGTH_SHORT)
                                .show()
                        }
                        transactionResponse.isBadRequest -> {
                            Toast.makeText(applicationContext, "Bad request", Toast.LENGTH_SHORT)
                                .show()
                        }
                        transactionResponse.isInternalError -> {
                            Toast.makeText(applicationContext, "Server error", Toast.LENGTH_SHORT)
                                .show()
                        }
                        transactionResponse.isProcessingError -> {
                            Toast.makeText(
                                applicationContext,
                                "Error processing data",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        transactionResponse.data != null -> {
                            startActivity(Intent(this, TransactionSuccesActivity::class.java))
                        }
                    }
                })
        }
    }

    companion object {
        const val TRANSACTION_DATA = "transaction_data"
    }
}
