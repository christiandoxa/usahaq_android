package com.ui.dashboard.ui.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.adapter.recommendation.location.LocationAdapter
import com.adapter.recommendation.price.PriceAdapter
import com.adapter.recommendation.trends.TrendsAdapter
import com.example.usahaq.model.CommonResponseModel
import com.facebook.shimmer.ShimmerFrameLayout
import com.model.MachineLearningData
import com.repo.response.MachineLearningResponse
import com.ui.recommendation.location.LocationActivity
import com.ui.recommendation.price.PriceRecommendationActivity
import com.ui.recommendation.trends.Trends
import com.ui.recommendation.trends.explore.ExploreActivity
import com.usahaq.usahaq.databinding.FragmentDashboardBinding
import com.util.ViewModelFactory
import com.viewmodel.MachineLearningViewModel
import com.viewmodel.TrendsViewModel

class DashboardFragment : Fragment() {

    private lateinit var binding: FragmentDashboardBinding
    private lateinit var machineLearningViewModel: MachineLearningViewModel
    private lateinit var trendsViewModel: TrendsViewModel
    private lateinit var adapter: PriceAdapter
    private lateinit var locationAdapter: LocationAdapter
    private lateinit var trendsAdapter: TrendsAdapter
    private lateinit var shimmerPrice: ShimmerFrameLayout
    private lateinit var shimmerTrends : ShimmerFrameLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDashboardBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val factory = ViewModelFactory.getInstance(requireActivity().application)
        machineLearningViewModel =
            ViewModelProvider(this, factory)[MachineLearningViewModel::class.java]
        trendsViewModel = ViewModelProvider(this, factory)[TrendsViewModel::class.java]
        adapter = PriceAdapter()
        locationAdapter = LocationAdapter()
        trendsAdapter = TrendsAdapter()
        binding.rvPriceDashboard.adapter = adapter
        binding.rvPriceDashboard.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvLocationDashboard.adapter = locationAdapter
        binding.rvLocationDashboard.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvCategories.adapter = trendsAdapter
        locationAdapter.setData()
        binding.rvCategories.layoutManager = GridLayoutManager(requireContext(), 2)
        fetchAllData()
        shimmerPrice = binding.shimmerLayout
        shimmerTrends = binding.shimmerTrends
        shimmerPrice.visibility = View.VISIBLE
        shimmerTrends.visibility = View.VISIBLE
        shimmerPrice.startShimmer()
        binding.apply {
            tvSeeAll2.setOnClickListener {
                startActivity(Intent(requireActivity(), PriceRecommendationActivity::class.java))
            }
            tvSeeAll.setOnClickListener {
                startActivity(Intent(requireActivity(), Trends::class.java))
            }

            tvSeeAll3.setOnClickListener {
                startActivity(Intent(requireActivity(), LocationActivity::class.java))
            }

            proCover.setOnClickListener {
                proCover.visibility = View.GONE
                tvProfeature.visibility = View.GONE
                tvUnlock.visibility = View.GONE
            }

            searchView.setOnClickListener {
                startActivity(Intent(requireActivity(), ExploreActivity::class.java))
            }
        }
        fetchTrendsSuggestion()
    }

    private fun fetchTrendsSuggestion() {
        trendsViewModel.getTrendsSuggestion().observe(viewLifecycleOwner, { result ->
            when {
                result.isUnauthorized -> {
                    Toast.makeText(context, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                result.isBadRequest -> {
                    Toast.makeText(context, "Bad request", Toast.LENGTH_SHORT).show()
                }
                result.isInternalError -> {
                    Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
                }
                result.isProcessingError -> {
                    Toast.makeText(context, "Error processing data", Toast.LENGTH_SHORT).show()
                }
                result.data != null -> {
                    var i = 0
                    val map = mutableMapOf<String, String>()
                    for ((k, v) in result.data!!.suggestions) {
                        map[k] = v
                        if (i == 3) break
                        i++
                    }
                    trendsAdapter.listTrends = map
                    trendsAdapter.notifyDataSetChanged()
                    shimmerTrends.stopShimmer()
                    shimmerTrends.visibility = View.GONE
                }
            }
        })
    }

    private fun fetchAllData() {
        fetchData("chilli")
        fetchData("meat")
        fetchData("chicken")
        fetchData("onion")
        fetchData("big_red_chilli")
        fetchData("garlic")
        fetchData("rice")
        fetchData("sugar")
        fetchData("oil")
        fetchData("egg")
    }

    private fun fetchData(modelType: String) {
        machineLearningViewModel.getPrediction(modelType)
            .observe(viewLifecycleOwner, { result ->
                processResponseData(result, modelType)
                shimmerPrice.stopShimmer()
                shimmerPrice.visibility = View.GONE
            })
    }

    private fun processResponseData(
        result: CommonResponseModel<MachineLearningResponse>,
        modelType: String
    ) {
        when {
            result.isUnauthorized -> {
                Toast.makeText(context, "Unauthorized", Toast.LENGTH_SHORT).show()
            }
            result.isBadRequest -> {
                Toast.makeText(context, "Bad request", Toast.LENGTH_SHORT).show()
            }
            result.isInternalError -> {
                Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
            }
            result.isProcessingError -> {
                Toast.makeText(context, "Error processing data", Toast.LENGTH_SHORT).show()
            }
            result.data != null -> {
                adapter.machineLearningDataList.add(
                    MachineLearningData(
                        modelType,
                        result.data!!.result
                    )
                )
                adapter.notifyDataSetChanged()
            }
        }
    }


}