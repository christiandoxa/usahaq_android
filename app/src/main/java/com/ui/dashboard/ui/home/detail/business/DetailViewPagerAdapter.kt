package com.ui.dashboard.ui.home.detail.business

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.repo.response.BusinessData
import com.ui.dashboard.ui.home.detail.business.tablayout.ProductFragment
import com.ui.dashboard.ui.home.detail.business.tablayout.PurchaseFragment
import com.ui.dashboard.ui.home.detail.business.tablayout.SalesFragment

class DetailViewPagerAdapter(
    activity: AppCompatActivity,
    private val businessData: BusinessData,
    val productListener: ProductFragment.ProductListener
) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> ProductFragment(businessData, productListener)
            1 -> PurchaseFragment()
            2 -> SalesFragment()
            else -> ProductFragment(businessData, productListener)
        }
    }
}