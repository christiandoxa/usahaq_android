package com.ui.dashboard.ui.home.transaction

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.repo.response.TransactionData
import com.usahaq.usahaq.databinding.ActivityTransactionNoteBinding
import com.usahaq.usahaq.databinding.SheetDeleteProductBinding
import com.usahaq.usahaq.databinding.SheetDeleteTransactionBinding

class TransactionNoteActivity : AppCompatActivity() {

    private lateinit var binding : ActivityTransactionNoteBinding
    private lateinit var sheetBehaviour: BottomSheetBehavior<View>
    private lateinit var sheet: SheetDeleteTransactionBinding
    private lateinit var sheetDialog: BottomSheetDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransactionNoteBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sheet = SheetDeleteTransactionBinding.inflate(layoutInflater)
        sheetBehaviour = BottomSheetBehavior.from(binding.bottomSheet)
        sheetDialog = BottomSheetDialog(this)
        sheetDialog.setContentView(sheet.root)

        binding.apply {
            btnBack.setOnClickListener { onBackPressed() }


            btnDelete.setOnClickListener {
                deleteProduct()
            }
        }
    }

    private fun loadData(transactionData: TransactionData){
        binding.apply {

        }
    }

    private fun deleteProduct(){
        if (sheetBehaviour.state == BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehaviour.state = BottomSheetBehavior.STATE_COLLAPSED
        }
        sheet.btnClose.setOnClickListener {
            sheetDialog.dismiss()
        }
        sheetDialog.window?.statusBarColor = 0x04000000
        sheetDialog.show()
    }

    companion object{
        const val TRANSACTION = "transaction"
    }
}