package com.ui.dashboard.ui.home.transaction

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.listener.TransactionInterface
import com.model.Transaction
import com.repo.response.MultipleProductResponse
import com.usahaq.usahaq.databinding.ActivityAddTransanctionBinding
import com.util.TransactionHelper

class AddTransactionActivity : AppCompatActivity(), TransactionInterface {
    private lateinit var binding: ActivityAddTransanctionBinding
    private var multipleProductResponse: MultipleProductResponse? = null
    private var transaction: Transaction? = null
    private lateinit var adapter : AddTransactionAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddTransanctionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        multipleProductResponse = intent.getParcelableExtra(PRODUCT_DATA)
        if(multipleProductResponse!=null){
            adapter = AddTransactionAdapter(this, multipleProductResponse!!, this)
            binding.viewpagerDetail.adapter = adapter
            binding.viewpagerDetail.offscreenPageLimit = adapter.itemCount
            TabLayoutMediator(
                binding.tableLayoutDetail,
                binding.viewpagerDetail
            ) { tab, position ->
                when (position) {
                    0 -> tab.text = "Catalog"
                    1 -> tab.text = "Warehouse"
                    3 -> tab.text = "Run Out"
                }
            }.attach()
        }



        binding.btnBack.setOnClickListener { onBackPressed() }

        binding.btnConfirmation.setOnClickListener {
            processTransaction()
        }
    }

    private fun processTransaction() {
        if (transaction == null) {
            Toast.makeText(applicationContext, "Please create a Transaction", Toast.LENGTH_SHORT)
                .show()
            return
        }
        val intent = Intent(this, TransactionSummaryActivity::class.java)
        intent.putExtra(TransactionSummaryActivity.TRANSACTION_DATA, transaction)
        startActivity(intent)
    }

    companion object {
        const val PRODUCT_DATA = "product_data"
    }

    override fun setTransaction(transaction: Transaction) {
        val isDifferentTransaction =
            TransactionHelper.checkTransactionDifference(this.transaction, transaction)
        if (isDifferentTransaction) Toast.makeText(
            applicationContext,
            "Transaction has been reset due to the change in transaction type",
            Toast.LENGTH_SHORT
        ).show()
        if (transaction.totalQuantity > 0) {
            val totalQuantity = transaction.totalQuantity
            binding.tvButtonQuantity.text =
                if (totalQuantity > 1) "$totalQuantity items" else "$totalQuantity item"
            binding.btnConfirmation.visibility = View.VISIBLE
            this.transaction = transaction
        } else {
            binding.btnConfirmation.visibility = View.GONE
            this.transaction = null
        }
    }
}