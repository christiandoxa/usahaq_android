package com.ui.dashboard.ui.home.transaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.adapter.transaction.TransactionProductAdapter
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.listener.TransactionInterface
import com.model.Transaction
import com.repo.response.ProductData
import com.usahaq.usahaq.databinding.ActivityAddTransanctionBinding
import com.usahaq.usahaq.databinding.FragmentAllBinding
import com.usahaq.usahaq.databinding.SheetProductBinding
import com.util.TransactionHelper

class CatalogFragment(
    private val productData: List<ProductData>,
    private val transactionInterface: TransactionInterface
) : Fragment(),
    TransactionProductAdapter.OnClickListener {

    private lateinit var binding: FragmentAllBinding
    private lateinit var sheetBehaviour: BottomSheetBehavior<View>
    private lateinit var sheetDialog: BottomSheetDialog
    private lateinit var sheet: SheetProductBinding
    private lateinit var activityBinding: ActivityAddTransanctionBinding
    private var transaction: Transaction = Transaction()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAllBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sheet = SheetProductBinding.inflate(layoutInflater)
        activityBinding = ActivityAddTransanctionBinding.inflate(layoutInflater)
        sheetBehaviour = BottomSheetBehavior.from(activityBinding.bottomSheet)
        sheetDialog = BottomSheetDialog(requireContext())
        sheetDialog.setContentView(sheet.root)
        binding.apply {
            rvProduct.adapter = TransactionProductAdapter(productData, this@CatalogFragment)
            rvProduct.layoutManager = GridLayoutManager(requireContext(), 3)
        }
    }

    override fun onClick(productData: ProductData) {
        TransactionHelper.showBottomSheetDialog(
            sheet,
            sheetDialog,
            sheetBehaviour,
            transaction,
            productData,
            requireContext(),
            transactionInterface
        )
    }
}