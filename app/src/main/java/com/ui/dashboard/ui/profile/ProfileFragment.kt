package com.ui.dashboard.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.glide.GlideApp
import com.ui.Login
import com.usahaq.usahaq.databinding.FragmentProfileBinding
import com.util.GlideListener
import com.util.ImageUrlHelper
import com.util.ViewModelFactory
import com.viewmodel.ProfileViewModel


class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private lateinit var viewModel: ProfileViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val factory = ViewModelFactory.getInstance(requireActivity().application)
        viewModel = ViewModelProvider(this, factory)[ProfileViewModel::class.java]
        viewModel.getAccount().observe(viewLifecycleOwner, { account ->

            when {
                account.isNotFound -> {
                    startActivity(Intent(context, Login::class.java))
                    requireActivity().finish()
                }
                account.isUnauthorized -> {
                    Toast.makeText(context, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                account.isInternalError -> {
                    Toast.makeText(context, "Server error", Toast.LENGTH_SHORT).show()
                }
                account.data != null -> {
                    binding.apply {
                        tvName.text = account.data!!.result.name
                        tvDetail.text = account.data!!.result.email
                        shimmerName.visibility = View.GONE
                        tvName.visibility = View.VISIBLE
                        shimmerDetail.visibility = View.GONE
                        tvDetail.visibility = View.VISIBLE
                    }
                    if (account.data!!.result.image != null) {
                        GlideApp.with(this).load(
                            ImageUrlHelper.getImageUrl(
                                requireContext(),
                                account.data!!.result.image!!
                            )
                        ).listener(GlideListener(binding.shimmerImage, binding.ivPoster))
                            .into(binding.ivPoster)
                    } else {
                        binding.shimmerImage.visibility = View.GONE
                        binding.ivPoster.visibility = View.VISIBLE
                    }
                }
            }
        })
    }
}