package com.ui.dashboard.ui.home.detail.business

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.tabs.TabLayoutMediator
import com.repo.response.BusinessData
import com.repo.response.MultipleProductResponse
import com.ui.dashboard.ui.home.AddProductActivity
import com.ui.dashboard.ui.home.detail.business.tablayout.ProductFragment
import com.ui.dashboard.ui.home.transaction.AddTransactionActivity
import com.usahaq.usahaq.databinding.ActivityDetailBusinessBinding

class DetailBusinessActivity : AppCompatActivity(), ProductFragment.ProductListener {

    private lateinit var binding: ActivityDetailBusinessBinding
    private var businessData: BusinessData? = null
    private var multipleProductResponse: MultipleProductResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBusinessBinding.inflate(layoutInflater)
        setContentView(binding.root)
        businessData = intent.getParcelableExtra(BUSINESS_DATA)

        val adapter = DetailViewPagerAdapter(this, businessData!!, this)

        binding.apply {

            cvClipboard.setOnClickListener {
                startActivity(Intent(this@DetailBusinessActivity, SalesSummary::class.java))
            }

            btnAddProduct.setOnClickListener {
                val intent = Intent(this@DetailBusinessActivity, AddProductActivity::class.java)
                intent.putExtra(AddProductActivity.ID_BUSINESS_DATA, businessData!!.id)
                startActivity(intent)
            }

            btnAddTransaction.setOnClickListener {
                val intent = Intent(this@DetailBusinessActivity, AddTransactionActivity::class.java)
                if (multipleProductResponse != null) {
                    intent.putExtra(AddTransactionActivity.PRODUCT_DATA, multipleProductResponse)
                }
                startActivity(intent)
            }

            btnBack.setOnClickListener {
                onBackPressed()
            }

            binding.viewpagerDetail.adapter = adapter
            binding.viewpagerDetail.offscreenPageLimit = adapter.itemCount
            TabLayoutMediator(
                binding.tableLayoutDetail,
                binding.viewpagerDetail
            ) { tab, position ->
                when (position) {
                    0 -> tab.text = "Product"
                    1 -> tab.text = "Purchase"
                    2 -> tab.text = "Sales"
                }
            }.attach()
        }
    }


    companion object {
        const val BUSINESS_DATA = "business_data"
    }

    override fun onProductFetched(multipleProductResponse: MultipleProductResponse) {
        this.multipleProductResponse = multipleProductResponse
    }
}
