package com.ui.dashboard.ui.home.transaction

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.listener.TransactionInterface
import com.repo.response.MultipleProductResponse
import com.ui.recommendation.price.tabLayout.ForYouFragment
import com.util.ProductHelper

class AddTransactionAdapter(
    activity: AppCompatActivity,
    val multipleProductResponse: MultipleProductResponse,
    private val transactionInterface: TransactionInterface
) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> CatalogFragment(
                ProductHelper.getCatalogProduct(multipleProductResponse),
                transactionInterface
            )
            1 -> WarehouseFragment(
                ProductHelper.getWarehouseProduct(multipleProductResponse),
                transactionInterface
            )
            else -> ForYouFragment()
        }
    }
}