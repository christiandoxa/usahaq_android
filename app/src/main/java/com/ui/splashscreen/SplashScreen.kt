package com.ui.splashscreen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.ui.intro.AppIntro
import com.usahaq.usahaq.R

class SplashScreen : AppCompatActivity() {

    private val splashDuration = 1500L

    /*lateinit var prefrence : SharedPreferences
    val pref_show_intro = "splashscreen"*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splashscreen)

        /*prefrence = getSharedPreferences("SplashScreen", Context.MODE_PRIVATE)*/

        Handler(mainLooper).postDelayed({

            startActivity(Intent(this, AppIntro::class.java))

            /*if(!prefrence.getBoolean(pref_show_intro, true)) {
                startActivity(Intent(this, DashboardActivity::class.java))
            }
            else {
                startActivity(Intent(this, AppIntro::class.java))
                val editor = prefrence.edit()
                editor.putBoolean(pref_show_intro, false)
                editor.apply()
            }
            finish()*/
        }, splashDuration)
    }


}