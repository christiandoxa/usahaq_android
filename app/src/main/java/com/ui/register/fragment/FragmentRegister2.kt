package com.ui.register.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.github.dhaval2404.imagepicker.ImagePicker
import com.listener.RegisterActivityListener
import com.ui.register.RegisterActivity
import com.usahaq.usahaq.databinding.FragmentRegister2Binding
import java.io.File

class FragmentRegister2(private val mRegisterActivityListener: RegisterActivityListener) :
    Fragment() {

    private lateinit var binding: FragmentRegister2Binding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegister2Binding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRegisterActivityListener.fragment2(binding)
        binding.apply {
            profileUser.setOnClickListener {
                pickImage()
            }
            tvAddPhoto.setOnClickListener {
                pickImage()
            }
            if(etAddress.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etAddress.error = "This field can't be empty"
            }
            if(etCity.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etCity.error = "This field can't be empty"
            }
            if(etIdNumber.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etIdNumber.error = "This field can't be empty"
            }
            if(etPostCode.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etPostCode.error = "This field can't be empty"
            }
        }
    }

    private fun pickImage() {
        ImagePicker.with(this)
            .compress(1024) //Final image size will be less than 1 MB(Optional)
            .maxResultSize(1080, 1080)
            .cropSquare() //Crop square image, its same as crop(1f, 1f)
            .start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                //Image Uri will not be null for RESULT_OK
                val fileUri = data?.data
                binding.profileUser.setImageURI(fileUri)
                val file: File = ImagePicker.getFile(data)!!
                mRegisterActivityListener.fragment2ImageChanged(file)
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(context, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(context, "Task Cancelled", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
