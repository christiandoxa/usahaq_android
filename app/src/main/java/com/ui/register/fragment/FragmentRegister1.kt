package com.ui.register.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.listener.RegisterActivityListener
import com.ui.Login
import com.ui.register.RegisterActivity
import com.usahaq.usahaq.databinding.FragmentRegister1Binding

class FragmentRegister1(private val mRegisterActivityListener: RegisterActivityListener) :
    Fragment() {

    private lateinit var binding: FragmentRegister1Binding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegister1Binding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRegisterActivityListener.fragment1(binding)

        binding.apply {
            if(etName.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etName.error = "This field can't be empty"
            }
            if(etDate.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etDate.error = "This field can't be empty"
            }
            if(etEmail.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etEmail.error = "This field can't be empty"
            }
            if(etPhoneNumber.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etPhoneNumber.error = "This field can't be empty"
            }
            if(etUsername.toString().trim().isEmpty()){
                RegisterActivity.isEmptyFields = true
                etUsername.error = "This field can't be empty"
            }

            tvbtnLogin.setOnClickListener {
                startActivity(Intent(requireContext(), Login::class.java))
            }
        }
    }
}