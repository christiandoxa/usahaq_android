package com.ui.register.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.listener.RegisterActivityListener
import com.usahaq.usahaq.databinding.FragmentRegister3Binding

class FragmentRegister3(private val mRegisterActivityListener: RegisterActivityListener) : Fragment() {

    private lateinit var binding : FragmentRegister3Binding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegister3Binding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mRegisterActivityListener.fragment3(binding)
    }
}