package com.ui.register

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.listener.RegisterActivityListener
import com.repo.request.AccountRequest
import com.ui.dashboard.DashboardActivity
import com.ui.register.fragment.FragmentRegister1
import com.ui.register.fragment.FragmentRegister2
import com.ui.register.fragment.FragmentRegister3
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ActivityRegisterBinding
import com.usahaq.usahaq.databinding.FragmentRegister1Binding
import com.usahaq.usahaq.databinding.FragmentRegister2Binding
import com.usahaq.usahaq.databinding.FragmentRegister3Binding
import com.util.ViewModelFactory
import com.viewmodel.RegisterViewModel
import java.io.File

class RegisterActivity : AppCompatActivity(), RegisterActivityListener {
    private var accountRequest: AccountRequest = AccountRequest()
    private lateinit var adapter: RegisterAdapter
    private lateinit var binding: ActivityRegisterBinding
    private lateinit var viewModel: RegisterViewModel
    private lateinit var mRegisterActivityListener: RegisterActivityListener
    private lateinit var fragment1Binding: FragmentRegister1Binding
    private lateinit var fragment2Binding: FragmentRegister2Binding
    private lateinit var fragment3binding: FragmentRegister3Binding
    private lateinit var auth: FirebaseAuth
    private lateinit var firebaseUser: FirebaseUser


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        auth = Firebase.auth
        firebaseUser = auth.currentUser!!
        mRegisterActivityListener = this
        val factory = ViewModelFactory.getInstance(application)
        viewModel = ViewModelProvider(this, factory)[RegisterViewModel::class.java]
        adapter = RegisterAdapter(supportFragmentManager)
        adapter.list.add(FragmentRegister1(mRegisterActivityListener))
        adapter.list.add(FragmentRegister2(mRegisterActivityListener))
        adapter.list.add(FragmentRegister3(mRegisterActivityListener))
        binding.apply {
            viewpager.adapter = adapter
            tvNumber.text = "1"
            tvNextAccept.text = "Next"
            btnNext.setOnClickListener {
                isEmptyFields = false
                if(!isEmptyFields) viewpager.currentItem++ }
            viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {

                }

                override fun onPageSelected(position: Int) {
                    if (position == adapter.list.size - 1) {
                        btnNext.setOnClickListener {

                            if(fragment3binding.cbTos.isChecked) register()
                        }
                    }
                    when (viewpager.currentItem) {
                        0 -> {
                            tvNumber.text = "1"
                            tvNextAccept.text = "Next"
                            indicator1.setTextColor(Color.WHITE)
                            indicator2.setTextColor(
                                ContextCompat.getColor(
                                    this@RegisterActivity,
                                    R.color.blue_button
                                )
                            )
                            indicator3.setTextColor(
                                ContextCompat.getColor(
                                    this@RegisterActivity,
                                    R.color.blue_button
                                )
                            )
                        }
                        1 -> {
                            accountRequest.name = fragment1Binding.etName.text.toString()
                            accountRequest.username = fragment1Binding.etUsername.text.toString()
                            accountRequest.birthDate = fragment1Binding.etDate.text.toString()
                            accountRequest.customPhoneNumber =
                                fragment1Binding.etPhoneNumber.text.toString()
                            accountRequest.customEmail = fragment1Binding.etEmail.text.toString()
                            tvNumber.text = "2"
                            tvNextAccept.text = "Next"
                            indicator1.setTextColor(
                                ContextCompat.getColor(
                                    this@RegisterActivity,
                                    R.color.blue_button
                                )
                            )
                            indicator2.setTextColor(Color.WHITE)
                            indicator3.setTextColor(
                                ContextCompat.getColor(
                                    this@RegisterActivity,
                                    R.color.blue_button
                                )
                            )
                        }
                        2 -> {
                            accountRequest.identityNumber =
                                fragment2Binding.etIdNumber.text.toString()
                            accountRequest.address = fragment2Binding.etAddress.text.toString()
                            accountRequest.city = fragment2Binding.etCity.text.toString()
                            accountRequest.postalCode = fragment2Binding.etPostCode.text.toString()
                            tvNumber.text = "3"
                            tvNextAccept.text = "Finish"
                            indicator1.setTextColor(
                                ContextCompat.getColor(
                                    this@RegisterActivity,
                                    R.color.blue_button
                                )
                            )
                            indicator2.setTextColor(
                                ContextCompat.getColor(
                                    this@RegisterActivity,
                                    R.color.blue_button
                                )
                            )
                            indicator3.setTextColor(Color.WHITE)
                        }
                    }
                }

                override fun onPageScrollStateChanged(state: Int) {
                }

            })
        }
    }

    private fun register() {
        viewModel.create(accountRequest).observe(this, { accountRequest ->
            when {
                accountRequest.isBadRequest -> {
                    Toast.makeText(this, "Bad request, please check your data", Toast.LENGTH_SHORT)
                        .show()
                }
                accountRequest.isUnauthorized -> {
                    Toast.makeText(this, "Unauthorized", Toast.LENGTH_SHORT).show()
                }
                accountRequest.isInternalError -> {
                    Toast.makeText(this, "Server error", Toast.LENGTH_SHORT).show()
                }
                accountRequest.isConflict -> {
                    Toast.makeText(this, "Your identity should be unique", Toast.LENGTH_SHORT)
                        .show()
                }
                accountRequest.isProcessingError -> {
                    Toast.makeText(this, "Local data processing error", Toast.LENGTH_SHORT).show()
                }
                accountRequest.data != null -> {
                    startActivity(
                        Intent(
                            this@RegisterActivity, DashboardActivity::class.java
                        )
                    )
                    finish()
                }
            }
        })
    }

    override fun fragment1(binding: FragmentRegister1Binding) {
        if (firebaseUser.phoneNumber != null && firebaseUser.phoneNumber!!.isNotEmpty()) {
            binding.etPhoneNumber.setText(firebaseUser.phoneNumber)
            binding.etPhoneNumber.isEnabled = false
        }
        if (firebaseUser.email != null && firebaseUser.email!!.isNotEmpty()) {
            binding.etEmail.setText(firebaseUser.email)
            binding.etEmail.isEnabled = false
        }
        fragment1Binding = binding
    }

    override fun fragment2(binding: FragmentRegister2Binding) {
        fragment2Binding = binding
    }

    override fun fragment3(binding : FragmentRegister3Binding){
        fragment3binding = binding
    }

    override fun fragment2ImageChanged(file: File) {
        accountRequest.image = file
    }

    companion object {
        var isEmptyFields = false
    }
}

class RegisterAdapter(manager: FragmentManager) :
    FragmentPagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    val list: MutableList<Fragment> = ArrayList()

    override fun getCount(): Int = list.size

    override fun getItem(position: Int): Fragment = list[position]


}
