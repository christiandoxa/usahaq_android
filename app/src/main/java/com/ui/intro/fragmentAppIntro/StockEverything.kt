package com.ui.intro.fragmentAppIntro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.usahaq.usahaq.databinding.FragmentStockEverythingBinding

class StockEverything : Fragment() {

    private lateinit var binding : FragmentStockEverythingBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStockEverythingBinding.inflate(layoutInflater)
        return binding.root
    }
}