package com.ui.intro.fragmentAppIntro

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.usahaq.usahaq.databinding.FragmentLetsGoBinding

class LetsGo : Fragment() {

    private lateinit var binding : FragmentLetsGoBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLetsGoBinding.inflate(layoutInflater)
        return binding.root
    }

}