package com.di

import android.app.Application
import com.repo.repository.account.AccountRemoteDataSource
import com.repo.repository.account.AccountRepository
import com.repo.repository.business.BusinessRemoteDataSource
import com.repo.repository.business.BusinessRepository
import com.repo.repository.machine_learning.MachineLearningRemoteDataSource
import com.repo.repository.machine_learning.MachineLearningRepository
import com.repo.repository.product.ProductRemoteDataSource
import com.repo.repository.product.ProductRepository
import com.repo.repository.transaction.TransactionRemoteDataSource
import com.repo.repository.transaction.TransactionRepository
import com.repo.repository.trends.TrendsRemoteDataSource
import com.repo.repository.trends.TrendsRepository

object Injection {
    fun provideAccountRepository(application: Application): AccountRepository {
        val remoteDataSource = AccountRemoteDataSource.getInstance(application)
        return AccountRepository.getInstance(remoteDataSource)
    }

    fun provideBusinessRepository(application: Application): BusinessRepository {
        val remoteDataSource = BusinessRemoteDataSource.getInstance(application)
        return BusinessRepository.getInstance(remoteDataSource)
    }

    fun provideProductRepository(application: Application): ProductRepository {
        val remoteDataSource = ProductRemoteDataSource.getInstance(application)
        return ProductRepository.getInstance(remoteDataSource)
    }

    fun provideMachineLearningRepository(application: Application): MachineLearningRepository {
        val remoteDataSource = MachineLearningRemoteDataSource.getInstance(application)
        return MachineLearningRepository.getInstance(remoteDataSource)
    }

    fun provideTrendsRepository(application: Application): TrendsRepository {
        val remoteDataSource = TrendsRemoteDataSource.getInstance(application)
        return TrendsRepository.getInstance(remoteDataSource)
    }

    fun provideTransactionRepository(application: Application): TransactionRepository {
        val remoteDataSource = TransactionRemoteDataSource.getInstance(application)
        return TransactionRepository(remoteDataSource)
    }
}
