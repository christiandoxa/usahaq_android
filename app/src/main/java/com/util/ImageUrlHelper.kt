package com.util

import android.content.Context
import android.net.Uri
import com.usahaq.usahaq.BuildConfig.BASE_URL
import com.usahaq.usahaq.R
import java.net.URL

class ImageUrlHelper {
    companion object {
        fun getImageUrl(context: Context, image: String): Uri {
            val accessToken =
                SharedPreferencesHelper.getString(context, context.getString(R.string.access_token))
            return Uri.parse(URL("$BASE_URL/assets?authorization=$accessToken&folder=images&file_name=$image").toString())
        }
    }
}
