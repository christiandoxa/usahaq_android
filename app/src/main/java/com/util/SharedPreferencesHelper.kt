package com.util

import android.content.Context
import com.usahaq.usahaq.R

class SharedPreferencesHelper {
    companion object {
        fun storeString(context: Context, key: String, data: String) {
            val sharedPref = context.getSharedPreferences(
                context.getString(R.string.app_name),
                Context.MODE_PRIVATE
            )
            val edit = sharedPref.edit()
            edit.putString(key, data)
            edit.apply()
        }

        fun getString(context: Context, key: String): String {
            val sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.app_name), Context.MODE_PRIVATE
            )
            return sharedPreferences.getString(key, "")!!
        }
    }
}