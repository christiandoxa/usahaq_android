package com.util

import com.example.usahaq.model.CommonResponseModel
import com.repo.repository.RequestCallback
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ResponseHelper<T>(private val callback: RequestCallback) : Callback<T> {
    override fun onResponse(call: Call<T>, response: Response<T>) {
        response.errorBody()?.let {
            when {
                response.code() == 400 -> {
                    callback.onResponse(
                        CommonResponseModel(
                            isBadRequest = true
                        )
                    )
                }
                response.code() == 401 -> {
                    callback.onResponse(
                        CommonResponseModel(
                            isUnauthorized = true
                        )
                    )
                }
                response.code() == 404 -> {
                    callback.onResponse(CommonResponseModel(isNotFound = true))
                }
                response.code() == 409 -> {
                    callback.onResponse(CommonResponseModel(isConflict = true))
                }
                else -> {
                    callback.onResponse(
                        CommonResponseModel(
                            isInternalError = true
                        )
                    )
                }
            }
        }
        response.body()?.let { result ->
            callback.onResponse(CommonResponseModel(data = result))
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        callback.onResponse(
            CommonResponseModel(
                isProcessingError = true,
                message = t.message
            )
        )
    }
}