package com.util

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.di.Injection
import com.viewmodel.*

class ViewModelFactory private constructor(private val application: Application) :
    ViewModelProvider.NewInstanceFactory() {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        when {
            modelClass.isAssignableFrom(LoginViewModel::class.java) -> {
                LoginViewModel(Injection.provideAccountRepository(application)) as T
            }
            modelClass.isAssignableFrom(RegisterViewModel::class.java) -> {
                RegisterViewModel(Injection.provideAccountRepository(application)) as T
            }
            modelClass.isAssignableFrom(ProfileViewModel::class.java) -> {
                ProfileViewModel(Injection.provideAccountRepository(application)) as T
            }
            modelClass.isAssignableFrom(BusinessViewModel::class.java) -> {
                BusinessViewModel(Injection.provideBusinessRepository(application)) as T
            }
            modelClass.isAssignableFrom(ProductViewModel::class.java) -> {
                ProductViewModel(Injection.provideProductRepository(application)) as T
            }
            modelClass.isAssignableFrom(MachineLearningViewModel::class.java) -> {
                MachineLearningViewModel(Injection.provideMachineLearningRepository(application)) as T
            }
            modelClass.isAssignableFrom(TrendsViewModel::class.java) -> {
                TrendsViewModel(Injection.provideTrendsRepository(application)) as T
            }
            modelClass.isAssignableFrom(TransactionViewModel::class.java) -> {
                TransactionViewModel(Injection.provideTransactionRepository(application)) as T
            }
            else -> throw Throwable("Unknown ViewModel class: " + modelClass.name)
        }

    companion object {
        @Volatile
        private var instance: ViewModelFactory? = null

        fun getInstance(application: Application): ViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: ViewModelFactory(application)
            }
    }
}
