package com.util

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.listener.TransactionInterface
import com.model.Transaction
import com.model.TransactionItem
import com.repo.response.ProductData
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.SheetProductBinding

class TransactionHelper {
    companion object {
        fun showBottomSheetDialog(
            sheet: SheetProductBinding,
            sheetDialog: BottomSheetDialog,
            sheetBehaviour: BottomSheetBehavior<View>,
            transaction: Transaction,
            productData: ProductData,
            context: Context,
            transactionInterface: TransactionInterface
        ) {
            val transactionItem: TransactionItem? = try {
                transaction.transactions.first { item -> item.productData == productData }
            } catch (_: Exception) {
                null
            }
            if (sheetBehaviour.state == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehaviour.state = BottomSheetBehavior.STATE_COLLAPSED
            }
            sheet.btnClose.setOnClickListener {
                sheetDialog.dismiss()
            }
            sheetDialog.show()
            sheet.apply {
                tvRion.text = productData.productName
                stockAmount.text = productData.stock.toString()
                priceAmount.text = productData.price.toString()
                tvPrice.text = productData.price.toString()
            }
            var number = 1
            sheet.etNotes.setText("")
            if (transactionItem != null) {
                number = transactionItem.quantity
                sheet.etNotes.setText(transactionItem.note)
            }
            sheet.btnPlus.setOnClickListener {
                number++
                setText(sheet, number)
                sheet.ivBtnColor.setColorFilter(
                    ContextCompat.getColor(
                        context,
                        R.color.light_blue
                    )
                )
            }
            sheet.btnMinus.setOnClickListener {
                if (number > 0) {
                    number--
                    setText(sheet, number)
                }
                if (number == 0) {
                    sheet.tvAdditems.text = "Remove"
                    sheet.tvButtonQuantity.text = "All items"
                    sheet.ivBtnColor.setColorFilter(
                        ContextCompat.getColor(
                            context,
                            R.color.red
                        )
                    )
                }
            }
            sheet.btnConfirmation.setOnClickListener {
                processProduct(sheet, transaction, productData, number, transactionInterface)
                sheetDialog.dismiss()
            }
            setText(sheet, number)
        }

        private fun processProduct(
            sheet: SheetProductBinding,
            transaction: Transaction,
            productData: ProductData,
            number: Int,
            transactionInterface: TransactionInterface
        ) {
            val transactionItem: TransactionItem? = try {
                transaction.transactions.first { item -> item.productData == productData }
            } catch (_: Exception) {
                null
            }
            val productToAddOrUpdate = TransactionItem(
                productData,
                number,
                sheet.etNotes.text.toString()
            )
            if (transactionItem == null) {
                if (number == 0) return
                transaction.transactions.add(
                    productToAddOrUpdate
                )
            } else {
                val index =
                    transaction.transactions.indexOfFirst { item -> item.productData == productData }
                if (index > -1) {
                    if (number == 0) {
                        transaction.transactions.removeAt(index)
                    } else {
                        transaction.transactions[index] = productToAddOrUpdate
                    }
                }
            }
            transaction.getTotalPriceAndQuantity()
            transactionInterface.setTransaction(transaction)
        }

        private fun setText(sheet: SheetProductBinding, number: Int) {
            sheet.tvAdditems.text = "Add Items"
            sheet.tvQuantityAmount.text = number.toString()
            sheet.tvButtonQuantity.text = number.toString() + " items"
        }

        fun checkTransactionDifference(old: Transaction?, new: Transaction): Boolean {
            if (old != null) {
                val oldIsSell = try {
                    old.transactions.first { transactionItem -> transactionItem.productData.isSell }.productData.isSell
                } catch (_: Exception) {
                    false
                }
                val newIsSell = try {
                    new.transactions.first { transactionItem -> transactionItem.productData.isSell }.productData.isSell
                } catch (_: Exception) {
                    false
                }
                return oldIsSell != newIsSell
            }
            return false
        }
    }
}