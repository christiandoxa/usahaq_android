package com.util

import com.repo.response.MultipleProductResponse
import com.repo.response.ProductData

class ProductHelper {
    companion object {
        fun getCatalogProduct(multipleProductResponse: MultipleProductResponse): List<ProductData> {
            return multipleProductResponse.products.filter { productData -> productData.isSell }
        }

        fun getWarehouseProduct(multipleProductResponse: MultipleProductResponse): List<ProductData> {
            return multipleProductResponse.products.filter { productData -> !productData.isSell }
        }
    }
}