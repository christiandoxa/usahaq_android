package com.util

import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

fun MultipartBody.Builder.addData(name: String, data: String?) {
    if (data == null) return
    this.addFormDataPart(name, data)
}

fun MultipartBody.Builder.addData(name: String, data: File?) {
    if (data == null) return
    this.addFormDataPart(
        name,
        data.name,
        data.readBytes().toRequestBody(
            contentType = FileUtils.getMimeType(data.path)!!.toMediaType()
        )
    )
}
