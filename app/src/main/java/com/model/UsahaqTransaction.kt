package com.model

import android.os.Parcelable
import com.repo.response.ProductData
import kotlinx.parcelize.Parcelize

@Parcelize
data class Transaction(
    val transactions: MutableList<TransactionItem> = ArrayList(),
    var totalPrice: Int = 0,
    var totalQuantity: Int = 0,
    var idPaymentMethod: String = "-1"
) : Parcelable {
    fun getTotalPriceAndQuantity() {
        var totalPrice = 0
        var totalQuantity = 0
        transactions.forEach { transactionItem ->
            totalPrice += transactionItem.quantity * transactionItem.productData.price
            totalQuantity += transactionItem.quantity
        }
        this.totalPrice = totalPrice
        this.totalQuantity = totalQuantity
    }
}

@Parcelize
data class TransactionItem(
    val productData: ProductData,
    val quantity: Int,
    val note: String
) : Parcelable
