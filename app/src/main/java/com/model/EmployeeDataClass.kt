package com.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EmployeeDataClass(
    var name : String ?= null,
    var position : String ?= null,
    var attendance : Double ?= null,
    var performance : Double ?= null,
    var employeeImage : Int ?= null,
    var present : Boolean ?= null
) : Parcelable
