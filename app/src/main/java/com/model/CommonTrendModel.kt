package com.model

data class CompareModel(
    var itemName: String,
    var commonTrend: List<CommonTrendModel>,
    var relatedQueries: List<CommonTrendModel>
)

data class CommonTrendModel(
    var name: String? = null,
    var percentage: Int? = null
)
