package com.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SummaryTitleModel (
    var titlePage : String,
    var titleDetails : String,
    var titleDaily : String,
        ) : Parcelable

data class SummaryModel(
    var date : String,
    var income : Int
)