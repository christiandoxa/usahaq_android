package com.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LocationModel(
    val city : String ?= null,
    val province : String ?= null,
    val cityImage : Int ?= null
) : Parcelable