package com.model

data class MachineLearningData(
    val name: String,
    val result: ArrayList<Float>
)