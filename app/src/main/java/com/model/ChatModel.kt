package com.model

data class ChatModel (
    var name : String ?= null,
    var chat : String ?= null,
    var numberChat : Int ?= null
        )