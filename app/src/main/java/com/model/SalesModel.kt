package com.model

data class SalesModel (
    var itemName : String,
    var itemAmount : Int,
    var itemDetail : List<SalesDetailModel>
        )

data class SalesDetailModel(
    var day : String,
    var date : String,
    var itemAmount: Int
)