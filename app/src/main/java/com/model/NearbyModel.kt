package com.model

data class NearbyModel (
    var shopName : String ?= null,
    var type : String ?= null,
    var location : String?= null,
    var rate : Double ?= null,
    var image : Int ?= null
        )