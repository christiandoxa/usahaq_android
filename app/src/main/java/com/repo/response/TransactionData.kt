package com.repo.response

import com.google.gson.annotations.SerializedName

data class TransactionData(
    @field:SerializedName("CREATED_AT") val createdAt: String,
    @field:SerializedName("ID_TRANSACTION") val idTransaction: Int,
    @field:SerializedName("ID_PAYMENT_METHOD") val idPaymentMethod: Int,
    @field:SerializedName("TOTAL_QUANTITY") val totalQuantity: Int,
    @field:SerializedName("TOTAL_PRICE") val totalPrice: Int,
    @field:SerializedName("DETAIL_TRANSACTIONs") val detailTransaction: List<DetailTransactionData>?,
    @field:SerializedName("ID_PAYMENT_METHOD_PAYMENT_METHOD") val paymentMethod: PaymentData?
)
