package com.repo.response

import com.google.gson.annotations.SerializedName

data class BusinessResponse(
    @field:SerializedName("business") val business: BusinessData
)
