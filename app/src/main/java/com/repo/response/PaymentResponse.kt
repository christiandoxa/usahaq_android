package com.repo.response

import com.google.gson.annotations.SerializedName

data class PaymentResponse(
    @field:SerializedName("paymentMethod") val paymentMethod: List<PaymentData>
)
