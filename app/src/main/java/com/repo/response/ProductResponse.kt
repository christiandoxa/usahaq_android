package com.repo.response

import com.google.gson.annotations.SerializedName

data class ProductResponse(
    @field:SerializedName("product") val product: ProductData
)
