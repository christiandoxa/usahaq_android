package com.repo.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class BusinessData(
    @field:SerializedName("ID_BUSINESS") val id: Int,
    @field:SerializedName("ID_OWNER") val idOwner: Int,
    @field:SerializedName("BUSINESS_NAME") val businessName: String,
    @field:SerializedName("ADDRESS") val address: String,
    @field:SerializedName("LATITUDE") val latitude: Float,
    @field:SerializedName("LONGITUDE") val longitude: Float,
    @field:SerializedName("BUSINESS_IMAGE") val businessImage: String? = null
) : Parcelable
