package com.repo.response

import com.google.gson.annotations.SerializedName


data class TrendsTodayResponse(
    @field:SerializedName("result") val trendsToday: List<String>
)

data class TrendsYearResponse(
    @field:SerializedName("result") val trendsYear: List<String>
)

data class TrendsSuggestionResponse(
    @field:SerializedName("result") val suggestions: Map<String, String>
)

data class TrendsResponse(
    @field:SerializedName("result") val trends: TrendsData
)

data class TrendsData(
    @field:SerializedName("interest_over_time") val interestOverTime: Map<String, List<Int>>,
    @field:SerializedName("related_queries") val relatedQuery: Map<String, Map<String, Int>>,
    @field:SerializedName("interest_by_region") val regionInterest: Map<String, Map<String, Int>>
)
