package com.repo.response

import com.google.gson.annotations.SerializedName

data class DetailTransactionData(
    @field:SerializedName("ID_TRANSACTION") val idTransaction: Int,
    @field:SerializedName("ID_PRODUCT") val idProduct: Int,
    @field:SerializedName("QUANTITY") val quantity: Int,
    @field:SerializedName("NOTE") val note: String,
    @field:SerializedName("ID_PRODUCT_PRODUCT") val productData: ProductData
)
