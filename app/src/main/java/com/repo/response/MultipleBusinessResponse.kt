package com.repo.response

import com.google.gson.annotations.SerializedName

data class MultipleBusinessResponse(
    @field:SerializedName("businesses") val businesses: ArrayList<BusinessData>
)
