package com.repo.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ProductData(
    @field:SerializedName("ID_PRODUCT") val idProduct: Int,
    @field:SerializedName("ID_BUSINESS") val idBusiness: Int,
    @field:SerializedName("PRODUCT_NAME") val productName: String,
    @field:SerializedName("PRICE") val price: Int,
    @field:SerializedName("STOCK") val stock: Int,
    @field:SerializedName("IS_SELL") val isSell: Boolean,
    @field:SerializedName("PRODUCT_IMAGE") val productImage: String?
) : Parcelable
