package com.repo.response

import com.google.gson.annotations.SerializedName

data class PaymentData(
    @field:SerializedName("ID_PAYMENT_METHOD") val idPaymentMethod: Int,
    @field:SerializedName("PAYMENT_METHOD") val paymentMethod: String
)
