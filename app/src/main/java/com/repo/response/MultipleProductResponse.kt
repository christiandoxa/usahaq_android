package com.repo.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class MultipleProductResponse(
    @field:SerializedName("products") val products: List<ProductData>
) : Parcelable
