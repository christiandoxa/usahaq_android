package com.repo.response

import com.google.gson.annotations.SerializedName

data class MultipleTransactionResponse(
    @field:SerializedName("transactions") val transactions: List<TransactionData>
)
