package com.repo.response

import com.google.gson.annotations.SerializedName

data class TransactionResponse(
    @field:SerializedName("transactions") val transaction: TransactionData
)
