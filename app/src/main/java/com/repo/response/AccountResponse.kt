package com.repo.response

import com.google.gson.annotations.SerializedName

data class AccountResponse(
    @field:SerializedName("account") val result: AccountData,
    @field:SerializedName("message") val message: String,
    @field:SerializedName("accessToken") val accessToken: String
)

data class AccountData(
    @field:SerializedName("ID_ACCOUNT") val id: Int? = 0,
    @field:SerializedName("FIREBASE_PHONE_ID") val firebase_phone_id: String? = null,
    @field:SerializedName("FIREBASE_EMAIL_ID") val firebase_email_id: String? = null,
    @field:SerializedName("IDENTITY_NUMBER") val id_number: String? = null,
    @field:SerializedName("NAME") val name: String? = null,
    @field:SerializedName("USERNAME") val username: String? = null,
    @field:SerializedName("PHONE_NUMBER") val phoneNumber: String? = null,
    @field:SerializedName("EMAIL") val email: String? = null,
    @field:SerializedName("BIRTH_DATE") val birth_date: String? = null,
    @field:SerializedName("ADDRESS") val address: String? = null,
    @field:SerializedName("CITY") val city: String? = null,
    @field:SerializedName("POSTAL_CODE") val postalCode: String? = null,
    @field:SerializedName("ACCOUNT_IMAGE") val image: String? = null
)