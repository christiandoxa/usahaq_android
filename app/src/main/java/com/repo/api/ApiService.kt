package com.repo.api

import com.repo.response.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiService {
    @POST("partner/account/sign_in")
    fun partnerSignIn(@Header("authorization") authorization: String): Call<AccountResponse>

    @POST("partner/account/create")
    fun partnerCreate(
        @Header("authorization") authorization: String,
        @Body params: RequestBody
    ): Call<AccountResponse>

    @GET("partner/account")
    fun getPartner(@Header("authorization") authorization: String): Call<AccountResponse>

    @POST("business/create")
    fun createBusiness(
        @Header("authorization") authorization: String,
        @Body params: RequestBody
    ): Call<BusinessResponse>

    @GET("business")
    fun getAllBusiness(@Header("authorization") authorization: String): Call<MultipleBusinessResponse>

    @POST("product/create")
    fun createProduct(
        @Header("authorization") authorization: String,
        @Body params: RequestBody
    ): Call<ProductResponse>

    @GET("product/{id_business}")
    fun getAllProducts(
        @Header("authorization") authorization: String,
        @Path(value = "id_business", encoded = true) idBusiness: String
    ): Call<MultipleProductResponse>

    @GET("machine_learning/{type}")
    fun getPrediction(
        @Header("authorization") authorization: String,
        @Path(value = "type", encoded = true) type: String
    ): Call<MachineLearningResponse>

    @POST("trends")
    fun getTrends(
        @Header("authorization") authorization: String,
        @Body params: RequestBody
    ): Call<TrendsResponse>

    @GET("trends/today")
    fun getTrendsToday(
        @Header("authorization") authorization: String
    ): Call<TrendsTodayResponse>

    @GET("trends/suggestions")
    fun getTrendsSuggestions(
        @Header("authorization") authorization: String
    ): Call<TrendsSuggestionResponse>

    @GET("trends/year/{year}")
    fun getTrendsYear(
        @Header("authorization") authorization: String,
        @Path(value = "year", encoded = true) year: String
    ): Call<TrendsYearResponse>

    @POST("transaction/create")
    fun createTransaction(
        @Header("authorization") authorization: String,
        @Body params: RequestBody
    ): Call<TransactionResponse>

    @GET("transaction")
    fun getTransactionByType(
        @Header("authorization") authorization: String,
        @QueryMap options: Map<String, String>
    ): Call<MultipleTransactionResponse>

    @GET("payment_method")
    fun getAllPaymentMethod(@Header("authorization") authorization: String): Call<PaymentResponse>
}
