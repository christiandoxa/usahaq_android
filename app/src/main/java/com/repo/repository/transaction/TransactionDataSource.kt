package com.repo.repository.transaction

import androidx.lifecycle.LiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.TransactionRequest
import com.repo.response.MultipleTransactionResponse
import com.repo.response.PaymentResponse
import com.repo.response.TransactionResponse

interface TransactionDataSource {
    fun createTransaction(transactionRequest: TransactionRequest): LiveData<CommonResponseModel<TransactionResponse>>

    fun getAllTransaction(isSell: Boolean? = null): LiveData<CommonResponseModel<MultipleTransactionResponse>>

    fun getAllPaymentMethod(): LiveData<CommonResponseModel<PaymentResponse>>
}