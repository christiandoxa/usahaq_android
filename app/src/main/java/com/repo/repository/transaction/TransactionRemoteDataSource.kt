package com.repo.repository.transaction

import android.app.Application
import android.content.Context
import com.repo.api.ApiConfig
import com.repo.repository.RequestCallback
import com.repo.request.TransactionRequest
import com.usahaq.usahaq.R
import com.util.ResponseHelper
import com.util.SharedPreferencesHelper

class TransactionRemoteDataSource(private val application: Application) {
    fun createTransaction(callback: RequestCallback, transactionRequest: TransactionRequest) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context,
            context.getString(R.string.access_token)
        )
        ApiConfig.getApiService().createTransaction(accessToken, transactionRequest.toRequestBody())
            .enqueue(ResponseHelper(callback))
    }

    fun getAllTransaction(callback: RequestCallback, isSell: Boolean?) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context,
            context.getString(R.string.access_token)
        )
        val map = mutableMapOf<String, String>()
        if (isSell != null) {
            map["is_sell"] = isSell.toString()
        }
        ApiConfig.getApiService().getTransactionByType(accessToken, map)
            .enqueue(ResponseHelper(callback))
    }

    fun getAllPaymentMethod(callback: RequestCallback) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context,
            context.getString(R.string.access_token)
        )
        ApiConfig.getApiService().getAllPaymentMethod(accessToken).enqueue(ResponseHelper(callback))
    }

    companion object {
        @Volatile
        private var instance: TransactionRemoteDataSource? = null

        fun getInstance(application: Application): TransactionRemoteDataSource =
            instance ?: synchronized(this) {
                instance ?: TransactionRemoteDataSource(application).apply { instance = this }
            }
    }
}