package com.repo.repository.transaction

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.TransactionRequest
import com.repo.response.MultipleTransactionResponse
import com.repo.response.PaymentResponse
import com.repo.response.TransactionResponse

@Suppress("UNCHECKED_CAST")
class TransactionRepository(private val remoteDataSource: TransactionRemoteDataSource) :
    TransactionDataSource {
    override fun createTransaction(transactionRequest: TransactionRequest): LiveData<CommonResponseModel<TransactionResponse>> {
        val transactionData = MutableLiveData<CommonResponseModel<TransactionResponse>>()
        remoteDataSource.createTransaction(
            { result -> transactionData.postValue(result as CommonResponseModel<TransactionResponse>) },
            transactionRequest
        )
        return transactionData
    }

    override fun getAllTransaction(isSell: Boolean?): LiveData<CommonResponseModel<MultipleTransactionResponse>> {
        val transactionData = MutableLiveData<CommonResponseModel<MultipleTransactionResponse>>()
        remoteDataSource.getAllTransaction(
            { result -> transactionData.postValue(result as CommonResponseModel<MultipleTransactionResponse>) },
            isSell
        )
        return transactionData
    }

    override fun getAllPaymentMethod(): LiveData<CommonResponseModel<PaymentResponse>> {
        val paymentData = MutableLiveData<CommonResponseModel<PaymentResponse>>()
        remoteDataSource.getAllPaymentMethod { result -> paymentData.postValue(result as CommonResponseModel<PaymentResponse>) }
        return paymentData
    }

    companion object {
        @Volatile
        private var instance: TransactionRepository? = null

        fun getInstance(remoteData: TransactionRemoteDataSource): TransactionRepository =
            instance ?: synchronized(this) {
                instance ?: TransactionRepository(remoteData).apply { instance = this }
            }
    }
}