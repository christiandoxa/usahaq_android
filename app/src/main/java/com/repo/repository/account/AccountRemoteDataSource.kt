package com.repo.repository.account

import android.app.Application
import android.content.Context
import com.example.usahaq.model.CommonResponseModel
import com.repo.api.ApiConfig
import com.repo.repository.RequestCallback
import com.repo.request.AccountRequest
import com.repo.response.AccountResponse
import com.usahaq.usahaq.R
import com.util.SharedPreferencesHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountRemoteDataSource(private val application: Application) {
    fun signIn(callback: RequestCallback) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context,
            context.getString(R.string.firebase_access_token)
        )
        ApiConfig.getApiService().partnerSignIn(accessToken)
            .enqueue(object : Callback<AccountResponse> {
                override fun onResponse(
                    call: Call<AccountResponse>,
                    response: Response<AccountResponse>
                ) {
                    response.errorBody()?.let {
                        when {
                            response.code() == 404 -> {
                                callback.onResponse(CommonResponseModel(isNotFound = true))
                            }
                            response.code() == 401 -> {
                                callback.onResponse(
                                    CommonResponseModel(
                                        isUnauthorized = true
                                    )
                                )
                            }
                            else -> {
                                callback.onResponse(
                                    CommonResponseModel(
                                        isInternalError = true
                                    )
                                )
                            }
                        }
                    }
                    response.body()?.let { result ->
                        SharedPreferencesHelper.storeString(
                            context,
                            context.getString(R.string.access_token),
                            "Bearer ${result.accessToken}"
                        )
                        callback.onResponse(CommonResponseModel(data = result))
                    }
                }

                override fun onFailure(call: Call<AccountResponse>, t: Throwable) {
                    callback.onResponse(
                        CommonResponseModel(
                            isProcessingError = true,
                            message = t.message
                        )
                    )
                }

            })
    }

    fun create(callback: RequestCallback, accountRequest: AccountRequest) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context,
            context.getString(R.string.firebase_access_token)
        )
        ApiConfig.getApiService().partnerCreate(
            authorization = accessToken,
            accountRequest.toRequestBody()
        )
            .enqueue(object : Callback<AccountResponse> {
                override fun onResponse(
                    call: Call<AccountResponse>,
                    response: Response<AccountResponse>
                ) {
                    response.errorBody()?.let {
                        when {
                            response.code() == 400 -> {
                                callback.onResponse(
                                    CommonResponseModel(
                                        isBadRequest = true
                                    )
                                )
                            }
                            response.code() == 404 -> {
                                callback.onResponse(CommonResponseModel(isNotFound = true))
                            }
                            response.code() == 401 -> {
                                callback.onResponse(
                                    CommonResponseModel(
                                        isUnauthorized = true
                                    )
                                )
                            }
                            response.code() == 409 -> {
                                callback.onResponse(CommonResponseModel(isConflict = true))
                            }
                            else -> {
                                callback.onResponse(
                                    CommonResponseModel(
                                        isInternalError = true
                                    )
                                )
                            }
                        }
                    }
                    response.body()?.let { result ->
                        SharedPreferencesHelper.storeString(
                            context,
                            context.getString(R.string.access_token),
                            "Bearer ${result.accessToken}"
                        )
                        callback.onResponse(CommonResponseModel(data = result))
                    }
                }

                override fun onFailure(call: Call<AccountResponse>, t: Throwable) {
                    callback.onResponse(
                        CommonResponseModel(
                            isProcessingError = true,
                            message = t.message
                        )
                    )
                }

            })
    }

    fun getAccount(callback: RequestCallback) {
        val context: Context = application.applicationContext
        val accessToken =
            SharedPreferencesHelper.getString(context, context.getString(R.string.access_token))
        ApiConfig.getApiService().getPartner(accessToken)
            .enqueue(object : Callback<AccountResponse> {
                override fun onResponse(
                    call: Call<AccountResponse>,
                    response: Response<AccountResponse>
                ) {
                    response.errorBody()?.let {
                        when {
                            response.code() == 404 -> {
                                callback.onResponse(CommonResponseModel(isNotFound = true))
                            }
                            response.code() == 401 -> {
                                callback.onResponse(
                                    CommonResponseModel(
                                        isUnauthorized = true
                                    )
                                )
                            }
                            else -> {
                                callback.onResponse(
                                    CommonResponseModel(
                                        isInternalError = true
                                    )
                                )
                            }
                        }
                    }
                    response.body()?.let { result ->
                        SharedPreferencesHelper.storeString(
                            context,
                            context.getString(R.string.access_token),
                            "Bearer ${result.accessToken}"
                        )
                        callback.onResponse(CommonResponseModel(data = result))
                    }
                }

                override fun onFailure(call: Call<AccountResponse>, t: Throwable) {
                    callback.onResponse(
                        CommonResponseModel(
                            isProcessingError = true,
                            message = t.message
                        )
                    )
                }

            })
    }

    companion object {
        @Volatile
        private var instance: AccountRemoteDataSource? = null

        fun getInstance(application: Application): AccountRemoteDataSource =
            instance ?: synchronized(this) {
                instance ?: AccountRemoteDataSource(application).apply { instance = this }
            }
    }
}