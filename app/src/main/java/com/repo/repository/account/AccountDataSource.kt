package com.repo.repository.account

import androidx.lifecycle.LiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.AccountRequest
import com.repo.response.AccountResponse

interface AccountDataSource {

    fun signIn(): LiveData<CommonResponseModel<AccountResponse>>

    fun create(accountRequest: AccountRequest): LiveData<CommonResponseModel<AccountResponse>>

    fun getAccount(): LiveData<CommonResponseModel<AccountResponse>>
}
