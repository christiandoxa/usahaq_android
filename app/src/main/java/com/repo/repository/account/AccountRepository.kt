package com.repo.repository.account

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.AccountRequest
import com.repo.response.AccountResponse

@Suppress("UNCHECKED_CAST")
class AccountRepository private constructor(private val remoteDataSource: AccountRemoteDataSource) :
    AccountDataSource {

    override fun signIn(): LiveData<CommonResponseModel<AccountResponse>> {
        val accountData = MutableLiveData<CommonResponseModel<AccountResponse>>()
        remoteDataSource.signIn { result -> accountData.postValue(result as CommonResponseModel<AccountResponse>) }
        return accountData
    }

    override fun create(accountRequest: AccountRequest): LiveData<CommonResponseModel<AccountResponse>> {
        val accountData = MutableLiveData<CommonResponseModel<AccountResponse>>()
        remoteDataSource.create(
            { result ->
                accountData.postValue(result as CommonResponseModel<AccountResponse>)
            },
            accountRequest
        )
        return accountData
    }

    override fun getAccount(): LiveData<CommonResponseModel<AccountResponse>> {
        val accountData = MutableLiveData<CommonResponseModel<AccountResponse>>()
        remoteDataSource.getAccount { result -> accountData.postValue(result as CommonResponseModel<AccountResponse>) }
        return accountData
    }

    companion object {
        @Volatile
        private var instance: AccountRepository? = null

        fun getInstance(remoteData: AccountRemoteDataSource): AccountRepository =
            instance ?: synchronized(this) {
                instance ?: AccountRepository(remoteData).apply { instance = this }
            }
    }
}