package com.repo.repository.machine_learning

import androidx.lifecycle.LiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.response.MachineLearningResponse

interface MachineLearningDataSource {
    fun getPricePrediction(type: String): LiveData<CommonResponseModel<MachineLearningResponse>>
}
