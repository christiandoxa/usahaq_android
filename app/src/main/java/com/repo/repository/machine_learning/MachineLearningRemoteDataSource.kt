package com.repo.repository.machine_learning

import android.app.Application
import android.content.Context
import com.repo.api.ApiConfig
import com.repo.repository.RequestCallback
import com.usahaq.usahaq.R
import com.util.ResponseHelper
import com.util.SharedPreferencesHelper

class MachineLearningRemoteDataSource(private val application: Application) {
    fun getPricePrediction(callback: RequestCallback, type: String) {
        val context: Context = application.applicationContext
        val accessToken =
            SharedPreferencesHelper.getString(context, context.getString(R.string.access_token))
        ApiConfig.getApiService().getPrediction(accessToken, type)
            .enqueue(ResponseHelper(callback))
    }

    companion object {
        @Volatile
        private var instance: MachineLearningRemoteDataSource? = null

        fun getInstance(application: Application): MachineLearningRemoteDataSource =
            instance ?: synchronized(this) {
                instance ?: MachineLearningRemoteDataSource(application).apply { instance = this }
            }
    }
}