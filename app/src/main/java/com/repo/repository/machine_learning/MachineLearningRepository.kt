package com.repo.repository.machine_learning

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.response.MachineLearningResponse

@Suppress("UNCHECKED_CAST")
class MachineLearningRepository(private val remoteData: MachineLearningRemoteDataSource) :
    MachineLearningDataSource {
    override fun getPricePrediction(type: String): LiveData<CommonResponseModel<MachineLearningResponse>> {
        val machineLearningData = MutableLiveData<CommonResponseModel<MachineLearningResponse>>()
        remoteData.getPricePrediction({ result ->
            machineLearningData.postValue(result as CommonResponseModel<MachineLearningResponse>)
        }, type)
        return machineLearningData
    }

    companion object {
        @Volatile
        private var instance: MachineLearningRepository? = null

        fun getInstance(remoteData: MachineLearningRemoteDataSource): MachineLearningRepository =
            instance ?: synchronized(this) {
                instance ?: MachineLearningRepository(remoteData).apply { instance = this }
            }
    }
}
