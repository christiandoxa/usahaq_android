package com.repo.repository.business

import android.app.Application
import android.content.Context
import com.repo.api.ApiConfig
import com.repo.repository.RequestCallback
import com.repo.request.BusinessRequest
import com.usahaq.usahaq.R
import com.util.ResponseHelper
import com.util.SharedPreferencesHelper

class BusinessRemoteDataSource(private val application: Application) {
    fun createBusiness(callback: RequestCallback, businessRequest: BusinessRequest) {
        val context: Context = application.applicationContext
        val accessToken =
            SharedPreferencesHelper.getString(context, context.getString(R.string.access_token))
        ApiConfig.getApiService().createBusiness(accessToken, businessRequest.toRequestBody())
            .enqueue(ResponseHelper(callback))
    }

    fun getAllBusiness(callback: RequestCallback) {
        val context: Context = application.applicationContext
        val accessToken =
            SharedPreferencesHelper.getString(context, context.getString(R.string.access_token))
        ApiConfig.getApiService().getAllBusiness(accessToken)
            .enqueue(ResponseHelper(callback))
    }

    companion object {
        @Volatile
        private var instance: BusinessRemoteDataSource? = null

        fun getInstance(application: Application): BusinessRemoteDataSource =
            instance ?: synchronized(this) {
                instance ?: BusinessRemoteDataSource(application).apply { instance = this }
            }
    }
}