package com.repo.repository.business

import androidx.lifecycle.LiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.BusinessRequest
import com.repo.response.BusinessResponse
import com.repo.response.MultipleBusinessResponse

interface BusinessDataSource {
    fun create(businessRequest: BusinessRequest): LiveData<CommonResponseModel<BusinessResponse>>

    fun getAllBusiness(): LiveData<CommonResponseModel<MultipleBusinessResponse>>
}