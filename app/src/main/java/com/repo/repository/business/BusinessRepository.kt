package com.repo.repository.business

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.BusinessRequest
import com.repo.response.BusinessResponse
import com.repo.response.MultipleBusinessResponse

@Suppress("UNCHECKED_CAST")
class BusinessRepository private constructor(private val remoteDataSource: BusinessRemoteDataSource) :
    BusinessDataSource {
    override fun create(businessRequest: BusinessRequest): LiveData<CommonResponseModel<BusinessResponse>> {
        val businessData = MutableLiveData<CommonResponseModel<BusinessResponse>>()
        remoteDataSource.createBusiness({ result ->
            businessData.postValue(result as CommonResponseModel<BusinessResponse>)
        }, businessRequest)
        return businessData
    }

    override fun getAllBusiness(): LiveData<CommonResponseModel<MultipleBusinessResponse>> {
        val businessData = MutableLiveData<CommonResponseModel<MultipleBusinessResponse>>()
        remoteDataSource.getAllBusiness { result ->
            businessData.postValue(result as CommonResponseModel<MultipleBusinessResponse>)
        }
        return businessData
    }

    companion object {
        @Volatile
        private var instance: BusinessRepository? = null

        fun getInstance(remoteData: BusinessRemoteDataSource): BusinessRepository =
            instance ?: synchronized(this) {
                instance ?: BusinessRepository(remoteData).apply { instance = this }
            }
    }
}
