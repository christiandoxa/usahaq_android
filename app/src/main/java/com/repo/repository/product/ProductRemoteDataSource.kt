package com.repo.repository.product

import android.app.Application
import android.content.Context
import com.repo.api.ApiConfig
import com.repo.repository.RequestCallback
import com.repo.request.ProductRequest
import com.usahaq.usahaq.R
import com.util.ResponseHelper
import com.util.SharedPreferencesHelper

class ProductRemoteDataSource(private val application: Application) {
    fun createProduct(callback: RequestCallback, productRequest: ProductRequest) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context,
            context.getString(R.string.access_token)
        )
        ApiConfig.getApiService().createProduct(accessToken, productRequest.toRequestBody())
            .enqueue(ResponseHelper(callback))
    }

    fun getAllProduct(callback: RequestCallback, idBusiness: String) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context,
            context.getString(R.string.access_token)
        )
        ApiConfig.getApiService().getAllProducts(accessToken, idBusiness)
            .enqueue(ResponseHelper(callback))
    }

    companion object {
        @Volatile
        private var instance: ProductRemoteDataSource? = null

        fun getInstance(application: Application): ProductRemoteDataSource =
            instance ?: synchronized(this) {
                instance ?: ProductRemoteDataSource(application).apply { instance = this }
            }
    }
}