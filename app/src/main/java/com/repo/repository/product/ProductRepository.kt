package com.repo.repository.product

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.ProductRequest
import com.repo.response.MultipleProductResponse
import com.repo.response.ProductResponse

@Suppress("UNCHECKED_CAST")
class ProductRepository private constructor(private val remoteDataSource: ProductRemoteDataSource) :
    ProductDataSource {
    override fun create(productRequest: ProductRequest): LiveData<CommonResponseModel<ProductResponse>> {
        val productData = MutableLiveData<CommonResponseModel<ProductResponse>>()
        remoteDataSource.createProduct({ result ->
            productData.postValue(result as CommonResponseModel<ProductResponse>)
        }, productRequest)
        return productData
    }

    override fun getAllProduct(idBusiness: String): LiveData<CommonResponseModel<MultipleProductResponse>> {
        val products = MutableLiveData<CommonResponseModel<MultipleProductResponse>>()
        remoteDataSource.getAllProduct({ result ->
            products.postValue(result as CommonResponseModel<MultipleProductResponse>)
        }, idBusiness)
        return products
    }

    companion object {
        @Volatile
        private var instance: ProductRepository? = null

        fun getInstance(remoteData: ProductRemoteDataSource): ProductRepository =
            instance ?: synchronized(this) {
                instance ?: ProductRepository(remoteData).apply { instance = this }
            }
    }
}
