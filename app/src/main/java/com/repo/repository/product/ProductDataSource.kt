package com.repo.repository.product

import androidx.lifecycle.LiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.ProductRequest
import com.repo.response.MultipleProductResponse
import com.repo.response.ProductResponse

interface ProductDataSource {
    fun create(productRequest: ProductRequest): LiveData<CommonResponseModel<ProductResponse>>
    fun getAllProduct(idBusiness: String): LiveData<CommonResponseModel<MultipleProductResponse>>
}
