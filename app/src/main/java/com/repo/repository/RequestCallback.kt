package com.repo.repository

import com.example.usahaq.model.CommonResponseModel

fun interface RequestCallback {
    fun onResponse(result: CommonResponseModel<Any>)
}