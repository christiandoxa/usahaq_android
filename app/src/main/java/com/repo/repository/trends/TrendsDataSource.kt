package com.repo.repository.trends

import androidx.lifecycle.LiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.TrendsRequest
import com.repo.response.TrendsResponse
import com.repo.response.TrendsSuggestionResponse
import com.repo.response.TrendsTodayResponse
import com.repo.response.TrendsYearResponse

interface TrendsDataSource {
    fun trends(trendsRequest: TrendsRequest): LiveData<CommonResponseModel<TrendsResponse>>

    fun trendsToday(): LiveData<CommonResponseModel<TrendsTodayResponse>>

    fun trendsSuggestions(): LiveData<CommonResponseModel<TrendsSuggestionResponse>>

    fun trendsYear(year: String): LiveData<CommonResponseModel<TrendsYearResponse>>
}
