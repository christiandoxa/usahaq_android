package com.repo.repository.trends

import android.app.Application
import android.content.Context
import com.repo.api.ApiConfig
import com.repo.repository.RequestCallback
import com.repo.request.TrendsRequest
import com.usahaq.usahaq.R
import com.util.ResponseHelper
import com.util.SharedPreferencesHelper

class TrendsRemoteDataSource(private val application: Application) {
    fun getTrends(callback: RequestCallback, trendsRequest: TrendsRequest) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context, context.getString(R.string.access_token)
        )
        ApiConfig.getApiService().getTrends(accessToken, trendsRequest.toRequestBody())
            .enqueue(ResponseHelper(callback))
    }

    fun getTrendsToday(callback: RequestCallback) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context, context.getString(R.string.access_token)
        )
        ApiConfig.getApiService().getTrendsToday(accessToken)
            .enqueue(ResponseHelper(callback))
    }

    fun getTrendsSuggestions(callback: RequestCallback) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context, context.getString(R.string.access_token)
        )
        ApiConfig.getApiService().getTrendsSuggestions(accessToken)
            .enqueue(ResponseHelper(callback))
    }

    fun getTrendsYear(callback: RequestCallback, year: String) {
        val context: Context = application.applicationContext
        val accessToken = SharedPreferencesHelper.getString(
            context, "access_token"
        )
        ApiConfig.getApiService().getTrendsYear(accessToken, year)
            .enqueue(ResponseHelper(callback))
    }

    companion object {
        @Volatile
        private var instance: TrendsRemoteDataSource? = null

        fun getInstance(application: Application): TrendsRemoteDataSource =
            instance ?: synchronized(this) {
                instance ?: TrendsRemoteDataSource(application).apply { instance = this }
            }
    }
}