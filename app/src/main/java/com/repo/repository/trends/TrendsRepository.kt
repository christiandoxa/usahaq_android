package com.repo.repository.trends

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.usahaq.model.CommonResponseModel
import com.repo.request.TrendsRequest
import com.repo.response.TrendsResponse
import com.repo.response.TrendsSuggestionResponse
import com.repo.response.TrendsTodayResponse
import com.repo.response.TrendsYearResponse

@Suppress("UNCHECKED_CAST")
class TrendsRepository(private val remoteDataSource: TrendsRemoteDataSource) : TrendsDataSource {
    override fun trends(trendsRequest: TrendsRequest): LiveData<CommonResponseModel<TrendsResponse>> {
        val trends = MutableLiveData<CommonResponseModel<TrendsResponse>>()
        remoteDataSource.getTrends(
            { result -> trends.postValue(result as CommonResponseModel<TrendsResponse>) },
            trendsRequest
        )
        return trends
    }

    override fun trendsToday(): LiveData<CommonResponseModel<TrendsTodayResponse>> {
        val trendsToday = MutableLiveData<CommonResponseModel<TrendsTodayResponse>>()
        remoteDataSource.getTrendsToday { result ->
            trendsToday.postValue(result as CommonResponseModel<TrendsTodayResponse>)
        }
        return trendsToday
    }

    override fun trendsSuggestions(): LiveData<CommonResponseModel<TrendsSuggestionResponse>> {
        val trendsSuggestions = MutableLiveData<CommonResponseModel<TrendsSuggestionResponse>>()
        remoteDataSource.getTrendsSuggestions { result -> trendsSuggestions.postValue(result as CommonResponseModel<TrendsSuggestionResponse>) }
        return trendsSuggestions
    }

    override fun trendsYear(year: String): LiveData<CommonResponseModel<TrendsYearResponse>> {
        val trendsYear = MutableLiveData<CommonResponseModel<TrendsYearResponse>>()
        remoteDataSource.getTrendsYear(
            { result ->
                trendsYear.postValue(result as CommonResponseModel<TrendsYearResponse>)
            }, year
        )
        return trendsYear
    }

    companion object {
        @Volatile
        private var instance: TrendsRepository? = null

        fun getInstance(remoteData: TrendsRemoteDataSource): TrendsRepository =
            instance ?: synchronized(this) {
                instance ?: TrendsRepository(remoteData).apply { instance = this }
            }
    }
}