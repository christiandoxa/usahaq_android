package com.repo.request

import okhttp3.RequestBody

interface BaseRequest {
    fun toRequestBody(): RequestBody
}
