package com.repo.request

import com.model.Transaction
import okhttp3.FormBody
import okhttp3.RequestBody

data class TransactionRequest(
    val transaction: Transaction
) : BaseRequest {
    override fun toRequestBody(): RequestBody {
        val requestBody = FormBody.Builder()
        transaction.transactions.sortBy { it.productData.idProduct }
        requestBody.addEncoded("id_payment_method", transaction.idPaymentMethod)
        transaction.transactions.forEach { transactionItem ->
            requestBody.addEncoded(
                "id_product",
                transactionItem.productData.idProduct.toString()
            )
            requestBody.addEncoded("quantity", transactionItem.quantity.toString())
            requestBody.addEncoded("note", transactionItem.note)
        }
        return requestBody.build()
    }
}
