package com.repo.request

import com.util.addData
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

data class AccountRequest(
    var name: String? = null,
    var username: String? = null,
    var birthDate: String? = null,
    var address: String? = null,
    var city: String? = null,
    var postalCode: String? = null,
    var identityNumber: String? = null,
    var customEmail: String? = null,
    var customPhoneNumber: String? = null,
    var image: File? = null
) : BaseRequest {
    override fun toRequestBody(): RequestBody {
        val requestBody = MultipartBody.Builder()
        requestBody.setType(MultipartBody.FORM)
        requestBody.addData("name", name)
        requestBody.addData("username", username)
        requestBody.addData("birth_date", birthDate)
        requestBody.addData("address", address)
        requestBody.addData("city", city)
        requestBody.addData("postal_code", postalCode)
        requestBody.addData("identity_number", identityNumber)
        requestBody.addData("custom_email", customEmail)
        requestBody.addData("custom_phone_number", customPhoneNumber)
        requestBody.addData("image", image)
        return requestBody.build()
    }
}
