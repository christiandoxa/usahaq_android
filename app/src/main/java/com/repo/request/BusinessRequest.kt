package com.repo.request

import com.util.addData
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

data class BusinessRequest(
    var businessName: String? = null,
    var address: String? = null,
    var latLng: String? = null,
    var image: File? = null,
) : BaseRequest {
    override fun toRequestBody(): RequestBody {
        val requestBody = MultipartBody.Builder()
        requestBody.setType(MultipartBody.FORM)
        requestBody.addData("business_name", businessName)
        requestBody.addData("address", address)
        requestBody.addData("lat_lng", latLng)
        requestBody.addData("image", image)
        return requestBody.build()
    }
}


