package com.repo.request

import okhttp3.FormBody
import okhttp3.RequestBody

data class TrendsRequest(val keywords: List<String>) : BaseRequest {
    override fun toRequestBody(): RequestBody {
        val requestBody = FormBody.Builder()
        keywords.forEach { keywords ->
            requestBody.addEncoded("keywords", keywords)
        }
        return requestBody.build()
    }
}
