package com.repo.request

import com.util.addData
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

data class ProductRequest(
    var idBusiness: Int? = null,
    var productName: String? = null,
    var price: Int? = null,
    var stock: Int? = null,
    var catalog: Boolean? = null,
    var image: File? = null
) : BaseRequest {
    override fun toRequestBody(): RequestBody {
        val requestBody = MultipartBody.Builder()
        requestBody.setType(MultipartBody.FORM)
        requestBody.addData("id_business", idBusiness.toString())
        requestBody.addData("product_name", productName)
        requestBody.addData("price", price.toString())
        requestBody.addData("stock", stock.toString())
        requestBody.addData("is_sell", catalog.toString() )
        requestBody.addData("image", image)
        return requestBody.build()
    }
}
