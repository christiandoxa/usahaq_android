package com.adapter.management

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.model.EmployeeDataClass
import com.ui.dashboard.ui.management.detail.EmployeeProfileDetailActivity
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ItemPresentBinding

class EmployeeAttandanceAdapter : RecyclerView.Adapter<EmployeeAttandanceAdapter.ViewHolder>() {

    private var employeeList = ArrayList<EmployeeDataClass>()

    fun setData(){
        employeeList.add(EmployeeDataClass(name = "Yuda", position = "Shop Keeper", attendance = 100.0,
        performance = 100.0, employeeImage = R.drawable.logo, present = true))
    }

    class ViewHolder(private val binding: ItemPresentBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data : EmployeeDataClass){
            binding.apply {
                tvUserName.text = data.name
                tvPresent.text = if(data.present == true) "Present" else "Absent"
                Glide.with(root)
                    .load(data.employeeImage)
                    .into(ivPoster)

                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, EmployeeProfileDetailActivity::class.java)
                    intent.putExtra(EmployeeProfileDetailActivity.EMPLOYEE, data)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    itemView.context.startActivity(intent)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        ItemPresentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(employeeList[position])
    }

    override fun getItemCount(): Int = employeeList.size
}