package com.adapter.recommendation.price

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ItemDayRecommendPriceBinding
import com.util.DateHelper

class PredictionAdapter : RecyclerView.Adapter<PredictionAdapter.ViewHolder>() {
    var machineLearningDataList = ArrayList<Float>()

    class ViewHolder(val binding: ItemDayRecommendPriceBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(machineLearningData: Float, position: Int) {
            binding.apply {
                tvDate.text = DateHelper.getNextDate(position + 1)
                tvDay.text = DateHelper.getDayName(position+1)
                tvPrice.text = itemView.context.getString(
                    R.string.format_price, machineLearningData
                )
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemDayRecommendPriceBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(machineLearningDataList[position], position)
    }

    override fun getItemCount(): Int {
        return machineLearningDataList.size
    }
}