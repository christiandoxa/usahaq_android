package com.adapter.recommendation.price

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.model.MachineLearningData
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ItemProductRecommendBinding

class PricePredictionAdapter(private val listener: OnClickListener) :
    RecyclerView.Adapter<PricePredictionAdapter.ViewHolder>() {
    val machineLearningDataList = ArrayList<MachineLearningData>()

    class ViewHolder(val binding: ItemProductRecommendBinding, val listener: OnClickListener) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(machineLearningData: MachineLearningData) {
            binding.apply {
                binding.root.setOnClickListener {
                    listener.onClick(machineLearningData)
                }
                tvProductName.text = machineLearningData.name
                tvPrice.text = itemView.context.getString(
                    R.string.format_price, machineLearningData.result[0]
                )
                var image : Int ?=null
                when(machineLearningData.name){
                    "onion" -> {image = R.drawable.onion}
                    "chilli" -> {image = R.drawable.chili}
                    "rice" -> {image = R.drawable.rice}
                    "big_red_chilli" -> {image = R.drawable.big_chilli}
                    "garlic" -> {image = R.drawable.garlic}
                    "meat" -> {image = R.drawable.meat}
                    "egg" -> {image = R.drawable.egg}
                    "chicken" -> {image = R.drawable.chicken}
                    "sugar" -> {image = R.drawable.sugar}
                    "oil" -> {image = R.drawable.oil}
                }
                Glide.with(itemView.context).load(image).into(imgProduct)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProductRecommendBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            listener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(machineLearningDataList[position])
    }

    override fun getItemCount(): Int {
        return machineLearningDataList.size
    }

    interface OnClickListener {
        fun onClick(machineLearningData: MachineLearningData)
    }
}
