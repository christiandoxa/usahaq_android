package com.adapter.recommendation.trends

import android.graphics.BlendMode
import android.graphics.BlendModeColorFilter
import android.graphics.PorterDuff
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.model.CommonTrendModel
import com.usahaq.usahaq.databinding.ItemRegionBinding

class InterestAdapter(private val commonTrendList: List<CommonTrendModel>, val color : Int) :
    RecyclerView.Adapter<InterestAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemRegionBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: CommonTrendModel, position: Int, color: Int) {
            binding.apply {
                tvCity.text = data.name
                tvNumber.text = (position + 1).toString()
                tvPercentageNumber.text = data.percentage.toString()
                percentageBar.progress = data.percentage!!
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    percentageBar.progressDrawable?.colorFilter =
                        BlendModeColorFilter(color, BlendMode.SRC_IN)
                }else{
                    percentageBar.progressDrawable?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemRegionBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(commonTrendList[position], position, color)
    }

    override fun getItemCount(): Int = commonTrendList.size
}