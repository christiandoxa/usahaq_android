package com.adapter.recommendation.trends

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.usahaq.usahaq.databinding.ItemTrendsBinding

class TrendsAdapter : RecyclerView.Adapter<TrendsAdapter.ViewHolder>() {

    var listTrends: Map<String, String> = mutableMapOf()

    class ViewHolder(val binding: ItemTrendsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(category: String) {
            binding.apply {
                tvCategories.text = category
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemTrendsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var i = 0
        for ((key, _) in listTrends) {
            if (i == position) {
                holder.bind(key)
                break
            }
            i++
        }
    }

    override fun getItemCount(): Int = listTrends.size
}