package com.adapter.recommendation.trends

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ui.recommendation.trends.YearTrendsActivity
import com.usahaq.usahaq.databinding.ItemYearInSearchBinding

class TrendsYearsAdapter : RecyclerView.Adapter<TrendsYearsAdapter.ViewHolder>() {

    private var yearList = ArrayList<Int>()

    fun setData() {
        yearList.add(2020)
        yearList.add(2019)
        yearList.add(2018)
        yearList.add(2017)
        yearList.add(2016)
    }

    class ViewHolder(private val binding: ItemYearInSearchBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: Int) {
            binding.apply {
                tvYearInSearch.text = "Year in search $data"
                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, YearTrendsActivity::class.java)
                    intent.putExtra(YearTrendsActivity.YEAR, data.toString())
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    itemView.context.startActivity(intent)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemYearInSearchBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(yearList[position])
    }

    override fun getItemCount(): Int  = yearList.size
}