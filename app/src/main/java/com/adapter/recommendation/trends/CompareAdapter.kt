package com.adapter.recommendation.trends

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ui.recommendation.trends.explore.ExploreActivity
import com.usahaq.usahaq.databinding.ItemCompareBinding

class CompareAdapter(private val activity : ExploreActivity): RecyclerView.Adapter<CompareAdapter.ViewHolder>() {

    var topicList: List<String> = ArrayList()

    class ViewHolder(private val binding : ItemCompareBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data : String, activity: ExploreActivity, position: Int){
            binding.apply {
                tvExploreName.text = data
                btnDelete.setOnClickListener {
                    activity.deleteKeywords(position)
                }
                btnEdit.setOnClickListener {
                    activity.editQuery(position)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemCompareBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(topicList[position], activity, position)
    }

    override fun getItemCount(): Int = topicList.size
}