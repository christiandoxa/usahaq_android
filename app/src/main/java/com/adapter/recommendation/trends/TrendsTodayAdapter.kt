package com.adapter.recommendation.trends

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.usahaq.usahaq.databinding.ItemTrendsTodayBinding

class TrendsTodayAdapter : RecyclerView.Adapter<TrendsTodayAdapter.ViewHolder>() {

    var trendsTodayList : List<String> = ArrayList()

    class ViewHolder(private val binding : ItemTrendsTodayBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data : String, position: Int) {
            binding.apply {
                tvTrendsName.text = data
                tvRankSearch.text = (position+1).toString()
                tvNumberSearch.text = "20k+ searches"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemTrendsTodayBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ))
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(trendsTodayList[position], position)
    }

    override fun getItemCount(): Int = trendsTodayList.size
}