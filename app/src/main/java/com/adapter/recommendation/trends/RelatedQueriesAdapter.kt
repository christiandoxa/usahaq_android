package com.adapter.recommendation.trends

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.model.CommonTrendModel
import com.ui.recommendation.trends.explore.ExploreActivity
import com.usahaq.usahaq.databinding.ItemRelatedqueriesBinding

class RelatedQueriesAdapter(
    private val relatedQueriesList: List<CommonTrendModel>,
    private val activity: ExploreActivity
) : RecyclerView.Adapter<RelatedQueriesAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemRelatedqueriesBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: CommonTrendModel, activity: ExploreActivity) {
            binding.apply {
                tvNumber.text = data.percentage.toString()
                tvRelatedQueries.text = data.name
                itemView.setOnClickListener {
                    activity.showRelatedDialog(data)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemRelatedqueriesBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(relatedQueriesList[position], activity)
    }

    override fun getItemCount(): Int = relatedQueriesList.size
}