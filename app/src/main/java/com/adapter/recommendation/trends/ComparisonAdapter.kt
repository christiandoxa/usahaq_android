package com.adapter.recommendation.trends

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.model.CommonTrendModel
import com.model.CompareModel
import com.repo.response.TrendsData
import com.ui.recommendation.trends.explore.ExploreActivity
import com.usahaq.usahaq.databinding.ItemParentrecyclerBinding

class ComparisonAdapter(private val activity: ExploreActivity) :
    RecyclerView.Adapter<ComparisonAdapter.ViewHolder>() {

    var listComparison = ArrayList<CompareModel>()

    fun setData(keywords: List<String>, trends: TrendsData) {
        for (keyword in keywords) {
            val regions = ArrayList<CommonTrendModel>()
            val region = trends.regionInterest[keyword]
            for ((key, value) in region!!) {
                regions.add(CommonTrendModel(key, value))
            }
            val relatedQueries = ArrayList<CommonTrendModel>()
            val query = trends.relatedQuery[keyword]
            for ((key, value) in query!!) {
                relatedQueries.add(CommonTrendModel(key, value))
            }
            listComparison.add(
                CompareModel(
                    keyword,
                    regions,
                    relatedQueries
                )
            )
        }
    }

    class ViewHolder(private val binding: ItemParentrecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val colors = arrayOf(Color.BLUE, Color.RED, Color.GREEN, Color.YELLOW, Color.BLACK)

        fun bind(data: CompareModel, activity: ExploreActivity, position: Int) {
            binding.apply {
                tvItem.text = data.itemName
                rvRegion.apply {
                    layoutManager = LinearLayoutManager(itemView.context)
                    adapter = InterestAdapter(data.commonTrend, colors[position])
                }
                rvRelatedQueries.apply {
                    layoutManager = LinearLayoutManager(itemView.context)
                    adapter = RelatedQueriesAdapter(data.relatedQueries, activity)
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(
        ItemParentrecyclerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listComparison[position], activity, position)
    }

    override fun getItemCount(): Int = listComparison.size
}