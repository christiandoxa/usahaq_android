package com.adapter.recommendation.trends

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.usahaq.usahaq.databinding.ItemDetailyeartrendsBinding

class TrendsYearDetailAdapter : RecyclerView.Adapter<TrendsYearDetailAdapter.ViewHolder>() {

    var trendsYearList: List<String> = ArrayList()

    class ViewHolder(private val binding: ItemDetailyeartrendsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data: String, position: Int) {
            binding.apply {
                tvTopic.text = data
                tvNumber.text = (position + 1).toString()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemDetailyeartrendsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(trendsYearList[position], position)
    }

    override fun getItemCount(): Int = trendsYearList.size
}