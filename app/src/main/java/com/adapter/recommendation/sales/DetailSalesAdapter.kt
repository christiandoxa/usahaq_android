package com.adapter.recommendation.sales

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.model.SalesDetailModel
import com.usahaq.usahaq.databinding.ItemDayRecommendPriceBinding
import com.util.DateHelper

class DetailSalesAdapter : RecyclerView.Adapter<DetailSalesAdapter.ViewHolder>() {

    var listDetail = ArrayList<Float>()


    class ViewHolder(val binding: ItemDayRecommendPriceBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(data : Float, position: Int){
            binding.apply {
                tvDate.text = DateHelper.getNextDate(position + 1)
                tvDay.text = DateHelper.getDayName(position+1)
                tvPrice.text = data.toString()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemDayRecommendPriceBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listDetail[position], position)
    }

    override fun getItemCount(): Int = listDetail.size
}