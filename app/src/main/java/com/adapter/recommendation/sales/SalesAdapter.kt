package com.adapter.recommendation.sales

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.model.MachineLearningData
import com.model.SalesDetailModel
import com.model.SalesModel
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ItemProductRecommendBinding

class SalesAdapter(private val listener: OnClickListener) :
    RecyclerView.Adapter<SalesAdapter.ViewHolder>() {
        val machineLearningDataList = ArrayList<MachineLearningData>()

        class ViewHolder(val binding: ItemProductRecommendBinding, val listener: OnClickListener) :
            RecyclerView.ViewHolder(binding.root) {

            fun bind(machineLearningData: MachineLearningData) {
                binding.apply {
                    binding.root.setOnClickListener {
                        listener.onClick(machineLearningData)
                    }
                    tvProductName.text = machineLearningData.name
                    tvPrice.text = itemView.context.getString(
                        R.string.format_amount, machineLearningData.result[0]
                    )
                }
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            return ViewHolder(
                ItemProductRecommendBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                ),
                listener
            )
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.bind(machineLearningDataList[position])
        }

        override fun getItemCount(): Int {
            return machineLearningDataList.size
        }

        interface OnClickListener {
            fun onClick(machineLearningData: MachineLearningData)
        }
    }