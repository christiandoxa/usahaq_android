package com.adapter.recommendation.location

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.model.LocationModel
import com.ui.recommendation.location.LocationDetailActivity
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ItemLocationBinding

class LocationDetailAdapter : RecyclerView.Adapter<LocationDetailAdapter.ViewHolder>() {

    private var listLocation = ArrayList<LocationModel>()

    fun setData(){
        listLocation.add(LocationModel("Malang", "Jawa Timur", R.drawable.malang))
        listLocation.add(LocationModel("Surabaya", "Jawa Timur", R.drawable.surabaya))
        listLocation.add(LocationModel("Jakarta", "DKI Jakarta", R.drawable.jakarta))
        listLocation.add(LocationModel("Semarang", "Jawa Tengah", R.drawable.semarang))
        listLocation.add(LocationModel("Yogyakarta", "DI Yogyakarta", R.drawable.jogja))
        listLocation.add(LocationModel("Denpasar", "Bali", R.drawable.denpasar))
    }

    class ViewHolder(val binding: ItemLocationBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(location: LocationModel) {
            binding.apply {
                tvCity.text = location.city
                tvProvince.text = location.province
                Glide.with(binding.root).load(location.cityImage).into(ivLocation)
                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, LocationDetailActivity::class.java)
                    intent.putExtra(LocationDetailActivity.CITY, location)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    itemView.context.startActivity(intent)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemLocationBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listLocation[position])
    }

    override fun getItemCount(): Int = listLocation.size
}