package com.adapter.recommendation.location

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.model.NearbyModel
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ItemNearbyBinding

class NearbyAdapter: RecyclerView.Adapter<NearbyAdapter.ViewHolder>() {

    private var listNearby = ArrayList<NearbyModel>()

    fun setData(){
        listNearby.add(NearbyModel("Seblak Alexander", "Snacks", "Pujasera Bunga Cokelat",5.0, R.drawable.seblak))
        listNearby.add(NearbyModel("Ricebok", "Beverages, Rice", "Pujasera Bunga Cokelat",5.0, R.drawable.ricebox))
        listNearby.add(NearbyModel("Teh Gelas", "Snacks", "Pujasera Bunga Cokelat",5.0, R.drawable.teh))
    }

    class ViewHolder(val binding: ItemNearbyBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(nearby : NearbyModel) {
            binding.apply {
                tvShopName.text = nearby.shopName
                tvType.text = nearby.type
                tvRate.text = nearby.rate.toString()
                tvAddress.text = nearby.location
                Glide.with(itemView.context).load(nearby.image).into(ivNearbypict)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemNearbyBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listNearby[position])
    }

    override fun getItemCount(): Int = listNearby.size
}