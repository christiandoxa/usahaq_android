package com.adapter.recommendation.location

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.model.LocationModel
import com.usahaq.usahaq.R
import com.usahaq.usahaq.databinding.ItemLocationdashboardBinding

class LocationAdapter : RecyclerView.Adapter<LocationAdapter.ViewHolder>() {

    private var listLocation = ArrayList<LocationModel>()

    fun setData(){
        listLocation.add(LocationModel("Malang", "Jawa Timur", R.drawable.city))
        listLocation.add(LocationModel("Surabaya", "Jawa Timur", R.drawable.secondcity))
    }

    class ViewHolder(val binding: ItemLocationdashboardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(location: LocationModel) {
            binding.apply {
                tvCity.text = location.city
                tvProvince.text = location.province
                Glide.with(binding.root).load(location.cityImage).into(ivLocation)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            ItemLocationdashboardBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listLocation[position])
    }

    override fun getItemCount(): Int = listLocation.size
}