package com.adapter.detailbusiness

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.repo.response.TransactionData
import com.ui.dashboard.ui.home.transaction.TransactionNoteActivity
import com.usahaq.usahaq.databinding.ItemPurchaseBinding
import com.util.ImageUrlHelper

class TransactionAdapter : RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {
    var purchaseData: List<TransactionData> = ArrayList()


    class ViewHolder(val binding: ItemPurchaseBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(detailTransactionData: TransactionData) {
            val context = binding.root.context
            binding.apply {
                tvProductName.text = detailTransactionData.createdAt.split(":")[0]
                tvPrice.text = detailTransactionData.totalPrice.toString()
                if (detailTransactionData.detailTransaction!![0].productData.productImage != null) {
                    Glide.with(context).load(
                        ImageUrlHelper.getImageUrl(
                            context,
                            detailTransactionData.detailTransaction[0].productData.productImage!!
                        )
                    ).into(imgProduct)
                }

                itemView.setOnClickListener {
                    itemView.context.startActivity(Intent(context, TransactionNoteActivity::class.java))
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemPurchaseBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(purchaseData[position])
    }

    override fun getItemCount(): Int = purchaseData.size
}