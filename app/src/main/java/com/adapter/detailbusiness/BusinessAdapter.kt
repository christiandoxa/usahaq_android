package com.adapter.detailbusiness

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.glide.GlideApp
import com.repo.response.BusinessData
import com.ui.dashboard.ui.home.detail.business.DetailBusinessActivity
import com.usahaq.usahaq.databinding.ItemBusinessBinding
import com.util.ImageUrlHelper
import com.util.ShimmerHelper

class BusinessAdapter : RecyclerView.Adapter<BusinessAdapter.ViewHolder>() {
    var businessData: ArrayList<BusinessData> = ArrayList()

    class ViewHolder(val binding: ItemBusinessBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(businessData: BusinessData, removeLine: Boolean) {
            val context = binding.root.context
            binding.tvBusinessName.text = businessData.businessName
            binding.tvBusinessLocation.text = businessData.address
            if (businessData.businessImage != null) {
                GlideApp.with(context).load(
                    ImageUrlHelper.getImageUrl(
                        context,
                        businessData.businessImage
                    )
                ).placeholder(ShimmerHelper.getShimmerPlaceHolder()).into(binding.ivPoster)
            }
            if (removeLine) {
                binding.line.visibility = View.GONE
            }
            binding.root.setOnClickListener {
                val intent = Intent(context, DetailBusinessActivity::class.java)
                intent.putExtra(DetailBusinessActivity.BUSINESS_DATA, businessData)
                context.startActivity(intent)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemBusinessBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(businessData[position], businessData.size - 1 == position)
    }

    override fun getItemCount(): Int {
        return businessData.size
    }
}
