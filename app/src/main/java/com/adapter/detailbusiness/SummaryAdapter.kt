package com.adapter.detailbusiness

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.model.SummaryModel
import com.usahaq.usahaq.databinding.ItemSummaryDetailBinding

class SummaryAdapter : RecyclerView.Adapter<SummaryAdapter.ViewHolder>() {
    var summaryData = ArrayList<SummaryModel>()

    fun setData(){
        summaryData.add(SummaryModel("Jan 1", (Math.random()*(200000 - 100000)).toInt()+100000))
        summaryData.add(SummaryModel("Jan 2", (Math.random()*(200000 - 100000)).toInt()+100000))
        summaryData.add(SummaryModel("Jan 3", (Math.random()*(200000 - 100000)).toInt()+100000))
        summaryData.add(SummaryModel("Jan 4", (Math.random()*(200000 - 100000)).toInt()+100000))
        summaryData.add(SummaryModel("Jan 5", (Math.random()*(200000 - 100000)).toInt()+100000))
        summaryData.add(SummaryModel("Jan 6", (Math.random()*(200000 - 100000)).toInt()+100000))
        summaryData.add(SummaryModel("Jan 7", (Math.random()*(200000 - 100000)).toInt()+100000))
        summaryData.add(SummaryModel("Jan 8", (Math.random()*(200000 - 100000)).toInt()+100000))
    }

    class ViewHolder(val binding: ItemSummaryDetailBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(productData: SummaryModel) {
            binding.apply {
                tvDate.text = productData.date
                tvDailySales.text = productData.income.toString()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemSummaryDetailBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(summaryData[position])
    }

    override fun getItemCount(): Int = summaryData.size
}