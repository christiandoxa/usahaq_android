package com.adapter.detailbusiness

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.glide.GlideApp
import com.repo.response.ProductData
import com.ui.dashboard.ui.home.detail.product.DetailProductActivity
import com.usahaq.usahaq.databinding.ItemProductBinding
import com.util.GlideListener
import com.util.ImageUrlHelper

class ProductAdapter : RecyclerView.Adapter<ProductAdapter.ViewHolder>() {
    var productData: List<ProductData> = ArrayList()

    class ViewHolder(val binding: ItemProductBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(productData: ProductData) {
            binding.apply {
                tvProductName.text = productData.productName
                tvPrice.text = productData.price.toString()
                if (productData.productImage != null) {
                    GlideApp.with(imgProduct.context).load(
                        ImageUrlHelper.getImageUrl(
                            imgProduct.context,
                            productData.productImage
                        )
                    ).listener(GlideListener(shimmerImg, imgProduct)).into(imgProduct)
                } else {
                    shimmerImg.stopShimmer()
                    shimmerImg.visibility = View.GONE
                    imgProduct.visibility = View.VISIBLE
                }
                itemView.setOnClickListener {
                    val intent = Intent(itemView.context, DetailProductActivity::class.java)
                    intent.putExtra(DetailProductActivity.PRODUCT, productData)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                    itemView.context.startActivity(intent)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProductBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(productData[position])
    }

    override fun getItemCount(): Int {
        return productData.size
    }
}
