package com.adapter.transaction

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.glide.GlideApp
import com.repo.response.ProductData
import com.usahaq.usahaq.databinding.ItemTransactionBinding
import com.util.ImageUrlHelper
import com.util.ShimmerHelper

class TransactionProductAdapter(
    private val productData: List<ProductData>,
    val listener: OnClickListener
) :
    RecyclerView.Adapter<TransactionProductAdapter.ViewHolder>() {
    class ViewHolder(val binding: ItemTransactionBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(productData: ProductData, listener: OnClickListener) {
            val context = binding.root.context
            binding.apply {
                tvProduct.text = productData.productName
                tvDescription.text = productData.price.toString()
                if (productData.productImage != null) {
                    GlideApp.with(context)
                        .load(ImageUrlHelper.getImageUrl(context, productData.productImage))
                        .placeholder(ShimmerHelper.getShimmerPlaceHolder()).into(ivFoodimage)
                }
                root.setOnClickListener {
                    listener.onClick(productData)
                }
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemTransactionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(productData[position], listener)
    }

    override fun getItemCount(): Int {
        return productData.size
    }

    interface OnClickListener {
        fun onClick(productData: ProductData)
    }
}