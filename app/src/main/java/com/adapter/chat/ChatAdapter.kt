package com.adapter.chat

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.model.ChatModel
import com.usahaq.usahaq.databinding.ItemChatBinding

class ChatAdapter : RecyclerView.Adapter<ChatAdapter.ViewHolder>() {

    private val chatList = ArrayList<ChatModel>()

    fun setData(){
        chatList.add(ChatModel(name = "Yuda", chat = "Payment Received", numberChat = 1))
    }

    class ViewHolder(private val binding : ItemChatBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(chat : ChatModel){
            binding.apply {
                tvUserName.text = chat.name
                tvChat.text = chat.chat
                tvNumberChat.text = chat.numberChat.toString()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(ItemChatBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(chatList[position])
    }

    override fun getItemCount(): Int = chatList.size
}